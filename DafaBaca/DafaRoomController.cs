﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DafaBaca;
using System.Collections;
using System.Reflection;
using System.Threading;

public class DafaRoomController
{
    public bool stop = true;

    public DafaRoomWidget widget;

    public Dictionary<string, BitArray> dicBetHistory = new Dictionary<string, BitArray>();
    public Dictionary<string, double> dicBetMoneyHistory = new Dictionary<string, double>();
    public Dictionary<string, BitArray> dicRetHistory = new Dictionary<string, BitArray>();
    public Dictionary<string, double> dicRetMoneyHistory = new Dictionary<string, double>();

    public string roomName = "";
    public int roomNum;
    public Room_STATE roomState = Room_STATE.Room_None;
    public int shou = 1;
    public bool changedShou = false;
    public int preRound = 0;
    public int round = 1;

    public string scoreP = "";
    public string scoreB = "";

    public int betNum = 0;
    public int betStep = 0;
    public int cntStepOut = 0;

    public bool preventionBet = false;

    public bool doDoubleBet = false;
    public int cntDoubleBet = 1;
    public int timeDoubleBet = 1;

    public DafaRoomController(DafaRoomWidget widget_ , int roomNum_)
    {
        widget = widget_;
        roomNum = roomNum_;
        roomName = widget.GetRoomName();

        stop = false;
    }
    public string GetPreKeyValue()
    {
        if (round == 1)
        {
            return (shou - 1) + "_" + preRound;
        }

        return shou + "_" + preRound;
    }

    public string GetCurKeyValue()
    {
        if (roomState == Room_STATE.Room_Result)
        {
            if (round == 0)
                return shou + "_" + preRound;
            else
                return shou + "_" + (round - 1);
        }

        if (round == 0)
            return shou + "_" + preRound;
        else
            return shou + "_" + round;
    }

    public void Process_State()
    {
        MethodInfo method = GetType().GetMethod(roomState.ToString() , BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy);
        if (method == null)
            return;

        method.Invoke(this , null);
    }

    public void UpdateInfo()
    {
        //최초 베팅타임은 패스
        if (roomState == Room_STATE.Room_None)
        {
            Room_STATE state = widget.GetRoomState();
            if (state != Room_STATE.Room_BetTime)
                roomState = state;
        }
        else
        {
            roomState = widget.GetRoomState();
        }
        
        round = int.Parse(widget.GetRound());

        if (round == 0 && (changedShou==false))
        {
            shou++;

#if TEST
#else
            if (shou%2 == 0)
            {
                Global.uiController.ClearRoomLog(roomNum);
                Global.uiController.PrintRoomLog(roomNum, "로그 정리");
            }
#endif

            Global.uiController.PrintRoomLog(roomNum, "슈 변경 : " + shou);
            changedShou = true;
        }

        if (round != 0)
        {
            changedShou = false;
        }
    }

    bool PreRoundCheck()
    {
        string curKey = GetPreKeyValue();
        if (dicRetHistory.Keys.Contains(curKey) == false && dicBetHistory.Keys.Contains(curKey))
        {
            Global.uiController.PrintRoomLog(roomNum, "이전판 결과 산정");

            BitArray ret;
            if (string.IsNullOrEmpty(scoreP) || string.IsNullOrEmpty(scoreB))
                ret = Global.MakeBitArr(widget.GetLastScore());            
            else
                ret = GetRetFromScore(false);


#if PRINT_LOG
            Global.uiController.PrintRoomLog(roomNum, curKey + " : 결과 Player : " + scoreP + " / Banker : " + scoreB);
#endif

            dicRetHistory.Add(curKey, ret);
            scoreP = "";
            scoreB = "";

            Global.uiController.PrintRoomLog(roomNum, curKey + " : 결과 / " + Global.GetStringRetBitArray(ret) + " 승");

            double winMoney = Global.GetWinMoney(dicBetMoneyHistory[curKey], dicBetHistory[curKey], ret);
            dicRetMoneyHistory.Add(curKey, winMoney);
            Global.uiController.PrintRoomLog(roomNum, curKey + " : 금액 / " + winMoney);

            BitArray comapre_ = (BitArray)ret.Clone();
            comapre_.And(dicBetHistory[curKey]);

            if (ret.Get((int)Type_Bet.Tie) == false)
            {
                if (Util.GetIntFromBitArray(comapre_) > 0)
                {
                    BetCommander.WinNextStep(this);
                }
                else
                {
                    BetCommander.LoseNextStep(this);
                }
            }
            return true;
        }

        return false;
    }

    public void Room_BetTime()
    {
        //슈 변경중
        if (round == 0)
        {
            //CheckLastRound();
            return;
        }
              
        string curKey = GetCurKeyValue();
        if (dicBetHistory.Keys.Contains(curKey))
            return;

        if (PreRoundCheck())
            curKey = GetCurKeyValue();

        BitArray betCommand = Global.MakeBitArr(BetCommander.CustomBet(betNum , betStep));
        dicBetHistory.Add(curKey , betCommand);

        double betMoney = BetCommander.BetMoney(betStep);
        if (doDoubleBet)
        {
            betMoney = betMoney * ((Math.Pow(2, timeDoubleBet) * 2) - 1);
            Global.uiController.PrintRoomLog(roomNum, curKey + " ," + cntDoubleBet + "번째 슈퍼더블베팅 횟수 " + timeDoubleBet + "번 : " + betMoney);
        }
        dicBetMoneyHistory.Add(curKey , betMoney);

        Process_Bet(curKey,betCommand, betMoney);
    }

    string StepToString()
    {
        string ret = "가상 ";
        if (Global.checkBox_Real)
            ret = "실제 ";
        return ret + (betNum + 1) + " - " + (betStep + 1);
    }

    public void Process_Bet(string curKey_, BitArray betCommand_ , double betMoney_)
    {
        if (Util.GetIntFromBitArray(betCommand_) <= 0)
        {
#if PRINT_LOG
            Global.uiController.PrintRoomLog(roomNum, curKey_ + " , 배팅 : 명령없음" );
#endif
        }
        else
        {
            Global.uiController.PrintRoomLog(roomNum, curKey_ + " , 배팅 : " + StepToString() + " / " + Global.GetStringRetBitArray(betCommand_) + " / 금액 : " + betMoney_);
        }

        if (betMoney_ <= 0)
            return;
        
        try
        {
            if (Global.checkBox_Real)
            {
                widget.DoBet(betCommand_, betMoney_ , true , false , 0);

                double betInfoFrom = widget.GetBetValue();
                Global.uiController.PrintRoomLog(roomNum, "총 배팅 : " + betInfoFrom);

//                 //betInfoFrom 값이 0이상이면 값 파싱이 일단은 됬다고 판다.
//                 if (betInfoFrom >= 0.0f && betInfoFrom < betMoney_)
//                 {
//                     double diff = betMoney_ - betInfoFrom;
//                     Global.uiController.PrintRoomLog(roomNum, "금액 매칭 : " + diff);
//                     widget.DoBet(betCommand_, diff, false);
//                 }
            }
            else
            {
                if (preventionBet)
                {
                    widget.PreventionBet();
                    preventionBet = false;
                }
            }
        }   
        catch
        {
            return;
        }       
    }

    public void ValidCheck()
    {

        string popupMsg = widget.RoomPopup();

        if (string.IsNullOrEmpty(popupMsg))
            return;

        Global.uiController.PrintRoomLog(roomNum, popupMsg);
        Global.uiController.PrintRoomLog(roomNum, "팝업 메시지로 인한 정지");

        stop = true;
    }

    public void Room_DealCard()
    {
        //scoreP = widget.GetPlayerScore();
        //scoreB = widget.GetBankerScore();
    }

    public BitArray GetRetFromScore(bool reParse_ = true)
    {
        if (reParse_)
        {
            scoreP = widget.GetPlayerScore();
            scoreB = widget.GetBankerScore();

            //Global.uiController.PrintRoomLog(roomNum, "점수 파싱 - P : " + scoreP + " / B : " + scoreB);
        }
        
       
        Type_Bet ret = Type_Bet.Nothing;

        int valueP = 0;
        if (string.IsNullOrEmpty(scoreP) == false)
            valueP = int.Parse(scoreP);

        int valueB = 0;
        if (string.IsNullOrEmpty(scoreB) == false)
            valueB = int.Parse(scoreB);


        if (valueP > valueB)
            ret = Type_Bet.Player;
        else if (valueP < valueB)
            ret = Type_Bet.Banker;
        else
            ret = Type_Bet.Tie;

        return Global.MakeBitArr(ret);
    }

    public void Room_Result()
    {
        stop = Global.stop;

        string curKey = GetCurKeyValue();

        if (dicBetHistory.Keys.Contains(curKey) 
            && dicRetHistory.Keys.Contains(curKey) == false)
        {
            Thread.Sleep(100);

            BitArray ret = GetRetFromScore();

            if (string.IsNullOrEmpty(scoreP) || string.IsNullOrEmpty(scoreB))
            {
                Global.uiController.PrintRoomLog(roomNum, curKey + "스코어보드 탐색");
                ret = Global.MakeBitArr(widget.GetLastScore());
            }
                

#if PRINT_LOG
            Global.uiController.PrintRoomLog(roomNum, curKey + " : 결과 Player : " + scoreP + " / Banker : " + scoreB);
#endif

            dicRetHistory.Add(curKey, ret);
            scoreP = "";
            scoreB = "";

            Global.uiController.PrintRoomLog(roomNum, curKey + " : 결과 / " + Global.GetStringRetBitArray(ret) + " 승");

            double winMoney = Global.GetWinMoney(dicBetMoneyHistory[curKey], dicBetHistory[curKey], ret);
            dicRetMoneyHistory.Add(curKey , winMoney);
            Global.uiController.PrintRoomLog(roomNum, curKey + " : 금액 / " + winMoney);

            BitArray comapre_ = (BitArray)ret.Clone();
            comapre_.And(dicBetHistory[curKey]);

            if (ret.Get((int)Type_Bet.Tie) == false)
            {
                if (Util.GetIntFromBitArray(comapre_) > 0)
                {
                    BetCommander.WinNextStep(this);
                }
                else
                {
                    BetCommander.LoseNextStep(this);
                }
            }

            preRound = round;
        }

        if (stop)
        {
            Global.uiController.PrintRoomLog(roomNum, "정지");
            roomState = Room_STATE.Room_None;
        }                 
    }

    public void InitData()
    {
#if TEST
        Global.uiController.PrintRoomLog(roomNum, "데이터 초기화");
#endif
        dicBetHistory.Clear();
        dicBetMoneyHistory.Clear();
        dicRetHistory.Clear();
        dicRetMoneyHistory.Clear();

        shou = 1;
        changedShou = false;
        preRound = 0;
        round = 1;
        scoreP = "";
        scoreB = "";
        betNum = 0;
        betStep = 0;
        cntStepOut = 0;

        preventionBet = false;
        doDoubleBet = false;
        cntDoubleBet = 1;
        timeDoubleBet = 1;

        stop = false;
    }
}

