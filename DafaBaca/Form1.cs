﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.Reflection;
using OpenQA.Selenium;
using System.Threading;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.IO;

namespace DafaBaca
{  
    public partial class Form1 : Form
    {
        [DllImport("USER32.DLL")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);
        [DllImport("user32.dll", SetLastError = true)]
        internal static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        Thread workThread = null;

        GameDafa gameDafa = null;

        Dictionary<int, DafaRoomViewer> dicViewer = new Dictionary<int, DafaRoomViewer>();

        System.Windows.Forms.Timer preventionTimeOut = new System.Windows.Forms.Timer();

        public Dictionary<int, ListBox> dicListBox = new Dictionary<int, ListBox>();

        public Form1()
        {
            InitializeComponent();
        }

        public void PrintSystemLog(string msg_)
        {
            listBox_LogSystem.Invoke(new MethodInvoker(delegate () {
                Util.AutoLineStringAdd(listBox_LogSystem, msg_);
            }));
        }

        public void PrintRoomLog(int num_, string msg_)
        {
            this.Invoke(new MethodInvoker(delegate () {
                Util.AutoLineStringAdd(dicListBox[num_], msg_);
            }));

            //PrintSystemLog(num_ + " 번방 : " + msg_);
        }

        public void ClearRoomLog(int num_)
        {
            this.Invoke(new MethodInvoker(delegate () {
                dicListBox[num_].Items.Clear();
            }));
        }

        void CheckTimeOut(object sender, EventArgs e)
        {
            if (Global.stop)
                return;

            if (Global.checkBox_PreventionTimeOut == false)
                return;

            IntPtr hwnd = FindWindow(null, "다파벳 라이브 딜러에서 라이브 온라인 카지노 게임을 플레이하세요! - Chrome");
            if (hwnd != IntPtr.Zero)
            {
                SetForegroundWindow(hwnd);
                SendKeys.SendWait("{F5}");
                PrintSystemLog("팅김방지");
            }

            Global.dicRoomController[1].preventionBet = true;
//             foreach (var item in Global.dicRoomController)
//                 item.Value.preventionBet = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            List<FieldInfo> listField = GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).ToList();
            List<FieldInfo> listRoomViewer = listField.FindAll(x => x.FieldType == typeof(DafaRoomViewer));
            for (int i = 0; i < listRoomViewer.Count; ++i)
            {
                DafaRoomViewer viewer = (DafaRoomViewer)listRoomViewer[i].GetValue(this);
                dicViewer[i + 1] = viewer;
            }

            for (int i=1; i<=4; ++i)
            {
                FieldInfo field = GetType().GetField("listBox_LogRoom" + i, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                if (field != null)
                {
                    ListBox listBox = (ListBox)field.GetValue(this);
                    dicListBox[i] = listBox;
                }
            }
                       
            SetStartPosition();

            preventionTimeOut.Interval = 5 * 1000 * 60;
            preventionTimeOut.Tick += new EventHandler(CheckTimeOut);
            preventionTimeOut.Start();

            metroButton_CreatPage.Enabled = false;
            metroButton_Start.Enabled = false;
            metroButton_Stop.Enabled = false;

            Util.KillProcess("chrome");
            EventUtil.ConnectCustomEvent(this);
            Global.uiController = this;          
        }

        public void UpdateUI(double totalRetMoney_ , int totalBetCnt_, int totalStepOut_)
        {
            this.Invoke(new MethodInvoker(delegate() {
                label_TotalRetMoney.Text = totalRetMoney_.ToString();
                label_TotalBetCnt.Text = totalBetCnt_.ToString();
                label_TotalStepOut.Text = totalStepOut_.ToString();
            }));
        }

        bool CheckGoalMoney(double totalRetMoney_)
        {
            if (Global.checkBox_GoalMoney == false)
                return false;

            if (totalRetMoney_ < Global.numericUpDown_GoalMoney)
                return false;
            
            Global.uiController.PrintSystemLog("목표수익 " + Global.numericUpDown_GoalMoney + " 도달 : " + totalRetMoney_);
            Global.uiController.PrintSystemLog("정지 중..");
            Global.goalMoneyStop = true;

            return true;         
        }

        bool CheckLimitLoseMoney(double totalRetMoney_)
        {
            if (Global.checkBox_LimitLoseMoney == false)
                return false;

            if (totalRetMoney_ > (Global.numericUpDown_LimitLoseMoney * -1))
                return false;

            Global.uiController.PrintSystemLog("한계손실 " + Global.numericUpDown_LimitLoseMoney + " 도달 : " + totalRetMoney_);
            Global.uiController.PrintSystemLog("정지 중..");
            Global.limitLoseMoneyStop = true;

            return true;
        }

        bool CheckLimitBetCnt(int totalBetCnt_)
        {
            if (Global.checkBox_LmitBetCnt == false)
                return false;

            if (totalBetCnt_ < Global.numericUpDown_LmitBetCnt)
                return false;

            Global.uiController.PrintSystemLog("제한판수 " + Global.numericUpDown_LmitBetCnt + " 도달 : " + totalBetCnt_);
            Global.uiController.PrintSystemLog("정지 중..");
            Global.limitBetCntStop = true;

            return true;
        }

        bool CheckLimitStepoutCnt(int totalStepOut_)
        {
            if (Global.checkBox_LmitStepoutCnt == false)
                return false;

            if (totalStepOut_ < Global.numericUpDown_LmitStepoutCnt)
                return false;

            Global.uiController.PrintSystemLog("제한카운트 " + Global.numericUpDown_LmitStepoutCnt + " 초과 : " + totalStepOut_);
            Global.uiController.PrintSystemLog("정지 중..");
            Global.limitStepoutCntStop = true;

            return true;
        }

        void CheckLimitBetnum()
        {
            if (Global.checkBox_LimitBetNum == false)
                return;

            foreach (var item in Global.dicRoomController)
            {
                if (item.Value.stop)
                    continue;

                if ((item.Value.betNum+1) < Global.numericUpDown_LimitBetNum)
                    continue;              
                Global.uiController.PrintSystemLog("제한번호 " + Global.numericUpDown_LimitBetNum + " 도달 : " + (item.Value.betNum+1));
                Global.uiController.PrintSystemLog(item.Value.roomNum + " 방 정지");

                item.Value.stop = true;                                                                
            }

            if (Global.AllStop())
                Global.stop = true;
        }

        public void StopCheck(double totalRetMoney_, int totalBetCnt_, int totalStepOut_)
        {
            if (Global.stop 
                || Global.goalMoneyStop
                || Global.limitLoseMoneyStop
                || Global.limitBetCntStop
                || Global.limitStepoutCntStop
                )
                return;

            if (CheckGoalMoney(totalRetMoney_))
                return;
            if (CheckLimitLoseMoney(totalRetMoney_))
                return;
            if (CheckLimitBetCnt(totalBetCnt_))
                return;
            if (CheckLimitStepoutCnt(totalStepOut_))
                return;

            //CheckLimitBetnum();
        }

        public void WorkThread()
        {
            if (Global.checkBox_CntReTry == false)
            {
                Global.numericUpDown_CntReTry = int.MaxValue;
            }

            for(int i=0; i<Global.numericUpDown_CntReTry; ++i)
            {
                if (Global.entireStop)
                    break;

                foreach (var item in Global.dicRoomController)
                    item.Value.InitData();

                Global.goalMoneyStop = false;
                Global.limitLoseMoneyStop = false;
                Global.limitBetCntStop = false;
                Global.limitStepoutCntStop = false;

                Global.stop = Global.entireStop;
#if TEST
                PrintSystemLog( i + "번째 시작");
#else
                PrintSystemLog("시작");
#endif

                while (true)
                {
                    if (Global.AllStop())
                        break;

                    foreach (var item in Global.dicRoomController)
                    {
                        item.Value.UpdateInfo();
                        dicViewer[item.Key].UpdateRoomViewer(item.Value);

                        if (item.Value.stop == false)
                            item.Value.ValidCheck();

                        if (item.Value.stop)
                            continue;

                        item.Value.Process_State();
                    }


                    double totalRetMoney = 0.0f;
                    int totalBetCnt = 0;
                    int totalStepOut = 0;

                    foreach (var item in Global.dicRoomController)
                    {
                        totalRetMoney += item.Value.dicRetMoneyHistory.Values.Sum();
                        totalBetCnt += item.Value.dicBetHistory.Values.ToList().FindAll(x => Util.GetIntFromBitArray(x) > 0).Count;
                        totalStepOut += item.Value.cntStepOut;
                    }

                    UpdateUI(totalRetMoney , totalBetCnt , totalStepOut);
                    StopCheck(totalRetMoney, totalBetCnt, totalStepOut);
                }

                PrintSystemLog("정지 완료");

                if (Global.checkBox_RetryDelay && Global.entireStop == false)
                {
                    PrintSystemLog("재시작 까지 남은 초 : " + Global.numericUpDown_RetryDelay);
                    Thread.Sleep(Global.numericUpDown_RetryDelay * 1000);
                }
            }

            PrintSystemLog("모든 작업 끝");

            this.Invoke(new MethodInvoker(delegate ()
            {
                metroButton_Stop.Enabled = false;
                metroButton_Start.Enabled = true;

                checkBox_Real.Enabled = true;
                checkBox_PreventionTimeOut.Enabled = true;
                checkBox_DoubleBet.Enabled = true;
                numericUpDown_CntDouble.Enabled = checkBox_DoubleBet.Checked;
                checkBox_Dollar.Enabled = true;

                metroButton_LoadSetting.Enabled = true;
                metroButton_SaveSetting.Enabled = true;
                tableLayoutPanel_Setting.Enabled = true;
                numericUpDown_Num.Enabled = true;
                numericUpDown_BaseMoney.Enabled = true;
            }));
        }

        private void metroButton_Start_Click(object sender, EventArgs e)
        {
            if (Global.settingInfo.dicCondition.Count <= 0)
            {
                PrintSystemLog("설정창에 베팅 설정을 해 주세요");
                return;
            }

            if (gameDafa == null)
            {
                PrintSystemLog("먼저 페이지를 생성해 주세요.");
                return;
            }

            if (workThread != null && workThread.IsAlive)
            {
                workThread.Abort();
                workThread = null;
            }

            IList<IWebElement> listRoom = gameDafa.GetGameFrameInfo();
            if (listRoom == null)
            {
                PrintSystemLog("게임 페이지에 진입 후 시작해 주세요");
                return;
            }

            if (gameDafa.CollectRoomWiget(listRoom) == false)
            {
                PrintSystemLog("모든게임을 뷰 전환해 주세요");
                return;
            }
          
            foreach (var item in Global.dicRoomController)
                item.Value.stop = false;

            metroButton_Start.Enabled = false;
            metroButton_Stop.Enabled = true;
            checkBox_Real.Enabled = false;
            checkBox_PreventionTimeOut.Enabled = false;
            checkBox_DoubleBet.Enabled = false;
            numericUpDown_CntDouble.Enabled = false;
            numericUpDown_TimeDouble.Enabled = false;
            checkBox_Dollar.Enabled = false;

            metroButton_LoadSetting.Enabled = false;
            metroButton_SaveSetting.Enabled = false;
            tableLayoutPanel_Setting.Enabled = false;
            numericUpDown_Num.Enabled = false;
            numericUpDown_BaseMoney.Enabled = false;

            workThread = new Thread(WorkThread);
            workThread.Start();

            Global.stop = false;
            Global.entireStop = false;
        }

        public void metroButton_Stop_Click(object sender, EventArgs e)
        {
            if (Global.stop)
                return;

            PrintSystemLog("정지중..");
            Global.stop = true;
            Global.entireStop = true;
        }

        private void metroButton_CreatPage_Click(object sender, EventArgs e)
        {
            gameDafa = new GameDafa();
            gameDafa.Process_Page();
            PrintSystemLog("로그인 후 게임 페이지로 이동해 주세요");

            metroButton_Start.Enabled = true;
        }

        private void metroButton_Setting_Click(object sender, EventArgs e)
        {
            SettingDlg settingDlg = new SettingDlg();
            settingDlg.Show();
            settingDlg.FormClosing += ExitSettingDlg;

            Util.SetFormPos(this, settingDlg, FORMPOS.FROMPOS_LEFT);

            metroButton_Setting.Enabled = false;

            if (string.IsNullOrEmpty(Global.curOptionFile))
            {
                
            }
            else
            {
                settingDlg.LoadOptionFromFile(Global.curOptionFile);
            }
        }

        void ExitSettingDlg(object sender, FormClosingEventArgs e)
        {
            if (Global.settingInfo.dicCondition.Count <= 0)
            {
                metroButton_Setting.Enabled = true;
                return;
            }
                

            metroButton_Setting.Enabled = true;
            metroButton_CreatPage.Enabled = true;

            numericUpDown_Num.Value = Global.settingInfo.endStep;
            numericUpDown_BaseMoney.Value = (int)Global.settingInfo.baseMoney;

        }

        void SetStartPosition()
        {
            StartPosition = FormStartPosition.Manual;
            Rectangle res = Screen.PrimaryScreen.Bounds;
            Location = new Point(res.Width - Size.Width, 0);
        }

        private void checkBox_DoubleBet_CheckedChanged(object sender, EventArgs e)
        {
            numericUpDown_CntDouble.Enabled = checkBox_DoubleBet.Checked;
            numericUpDown_TimeDouble.Enabled = checkBox_DoubleBet.Checked;
        }

        private void metroButton_LoadSetting_Click(object sender, EventArgs e)
        {
            string path = Directory.GetCurrentDirectory();
            path += "\\" + "Option";

            if (Directory.Exists(path) == false)
                Directory.CreateDirectory(path);

            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = path;
            ofd.Title = "설정";
            ofd.Filter = "|*.bofs";

            DialogResult dr = ofd.ShowDialog();

            if (dr == DialogResult.OK)
            {
                string fileName = ofd.SafeFileName;
                Global.LoadSettingFromFile(fileName);
                Global.curOptionFile = fileName;

                metroButton_Setting.Enabled = true;
                metroButton_CreatPage.Enabled = true;

                numericUpDown_Num.Value = Global.settingInfo.endStep;
                numericUpDown_BaseMoney.Value = (int)Global.settingInfo.baseMoney;

            }
            else if (dr == DialogResult.Cancel)
            {
                return;
            }
        }

        private void metroButton_SaveSetting_Click(object sender, EventArgs e)
        {
            if (Global.settingInfo.dicCondition.Count <= 0)
            {
                MessageBox.Show("설정된 정보가 없습니다.");
            }

            string fileName = Global.SaveSettingInfo(Global.settingInfo);
            if (string.IsNullOrEmpty(fileName) == false)
                MessageBox.Show(fileName + "이 저장 되었습니다.");
        }

        private void numericUpDown_Num_ValueChanged(object sender, EventArgs e)
        {
            Global.settingInfo.endStep = int.Parse(numericUpDown_Num.Value.ToString());
        }

        private void comboBox_BaseMoney_SelectedIndexChanged(object sender, EventArgs e)
        {
            Global.settingInfo.baseMoney = double.Parse(numericUpDown_BaseMoney.Value.ToString());
        }

        private void checkBox_RetryDelay_CheckedChanged(object sender, EventArgs e)
        {
            numericUpDown_RetryDelay.Enabled = checkBox_RetryDelay.Checked;
        }

        private void checkBox_GoalMoney_CheckedChanged(object sender, EventArgs e)
        {
            numericUpDown_GoalMoney.Enabled = checkBox_GoalMoney.Checked;
        }

        private void checkBox_LimitLoseMoney_CheckedChanged(object sender, EventArgs e)
        {
            numericUpDown_LimitLoseMoney.Enabled = checkBox_LimitLoseMoney.Checked;
        }

        private void checkBox_LmitBetCnt_CheckedChanged(object sender, EventArgs e)
        {
            numericUpDown_LmitBetCnt.Enabled = checkBox_LmitBetCnt.Checked;
        }

        private void checkBox_LmitStepoutCnt_CheckedChanged(object sender, EventArgs e)
        {
            numericUpDown_LmitStepoutCnt.Enabled = checkBox_LmitStepoutCnt.Checked;
        }

        private void checkBox_LimitBetNum_CheckedChanged(object sender, EventArgs e)
        {
            numericUpDown_LimitBetNum.Enabled = checkBox_LimitBetNum.Checked;
        }

        private void checkBox_CntReTry_CheckedChanged(object sender, EventArgs e)
        {
            numericUpDown_CntReTry.Enabled = checkBox_CntReTry.Checked;

            if (checkBox_CntReTry.Checked)
            {
                Global.numericUpDown_CntReTry = int.Parse(numericUpDown_CntReTry.Value.ToString());
            }
            else
            {
                Global.numericUpDown_CntReTry = int.MaxValue;
            }
        }
    }
}
