﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DafaBaca;
using OpenQA.Selenium;
using System.Collections.ObjectModel;
using System.Collections;
using System.Threading;

public class DafaRoomWidget : DriverController
{
    IWebElement rootFrame;

    IWebElement roomRoot;
    IWebElement bottomRoot;
    IWebElement topRoot;

    IWebElement bettingRoot;
    IWebElement playerRoot;
    IWebElement bankerRoot;

    IWebElement bottomLeft;

    IWebElement chipStackRoot;

    IWebElement mainRaod;

    IWebElement returnBtn;
    IWebElement doubleBtn;

    public Dictionary<double, IWebElement> dicChips = new Dictionary<double, IWebElement>();

    //Root
    //*[@id="root"]/div/div[1]/div/div/div[2]/div[2]/div[4]

    public DafaRoomWidget(IWebElement rootFrame_ , IWebElement roomRoot_)
    {
        rootFrame = rootFrame_;
        CollectRoomControllWidget(roomRoot_);
    }

    public void CheckNoActionStop()
    {
        try
        {
            string htmlString = roomRoot.GetAttribute("innerHTML");
            if (htmlString.Contains("wrapper--2a83p"))
            {
                IWebElement btn = roomRoot.FindElement(By.XPath(".//div/div[5]/div/button"));
                btn.Click();
                Global.uiController.PrintSystemLog("방 재 플레이");

                Thread.Sleep(1000);
            }
        }
        catch
        {

        }        
    }

    public void CollectRoomControllWidget(IWebElement roomRoot_)
    {
        roomRoot = roomRoot_;
        bottomRoot = roomRoot_.FindElement(By.ClassName("bottom-container--1aXuf"));
        topRoot = roomRoot_.FindElement(By.ClassName("top-container--HUqZI"));
        mainRaod = topRoot.FindElement(By.ClassName("bead-road--19BBc"));

        bettingRoot = bottomRoot.FindElement(By.ClassName("bettingGrid--1WBFi"));
        playerRoot = bettingRoot.FindElement(By.ClassName("player--3F1-C"));
        bankerRoot = bettingRoot.FindElement(By.ClassName("banker--11HzV"));
        bottomLeft = roomRoot_.FindElement(By.ClassName("bottom-left--28vAe"));

        chipStackRoot = bottomRoot.FindElement(By.ClassName("chips--3BVOI"));

        returnBtn = chipStackRoot.FindElement(By.XPath(".//li[1]/button"));
        doubleBtn = chipStackRoot.FindElement(By.ClassName("double--3Zz_1"));


        IList<IWebElement> listChips = chipStackRoot.FindElements(By.XPath(".//li"));

        dicChips.Clear();
        foreach (var item in listChips)
        {
            string className = item.GetAttribute("class");
            if (className.Contains("chip--NrbrM") == false)
                continue;
            
            IWebElement valueElm = item.FindElement(By.XPath(".//div[2]/div"));
            string value = valueElm.GetAttribute("data-value").Trim();
            double dValue = double.Parse(value);
            if (Global.checkBox_Dollar)
                dValue = dValue * 1000;

            dicChips.Add(dValue, item);
        }
        
    }

    public bool CanBetChip()
    {
        driver.SwitchTo().DefaultContent();
        driver.SwitchTo().Frame(rootFrame);

        foreach (var item in dicChips)
        {
            string className = item.Value.GetAttribute("class");
            if (className.Contains("hidden"))
                return false;
        }

        return true;
    }

    public string GetRound()
    {
        driver.SwitchTo().DefaultContent();
        driver.SwitchTo().Frame(rootFrame);
        return topRoot.FindElement(By.ClassName("count--8P7kD")).Text;
    }

    public string GetRemainMoney()
    {
        driver.SwitchTo().DefaultContent();
        driver.SwitchTo().Frame(rootFrame);
        return bottomLeft.FindElement(By.ClassName("amount--18dav")).Text;
    }

    public string GetLineNotice()
    {
        driver.SwitchTo().DefaultContent();
        driver.SwitchTo().Frame(rootFrame);
        return bottomRoot.FindElement(By.XPath(".//div[1]/div/div/div")).Text;
    }

    public string GetRoomName()
    {
        driver.SwitchTo().DefaultContent();
        driver.SwitchTo().Frame(rootFrame);
        return roomRoot.FindElement(By.ClassName("table-info--t_cum")).Text;
    }

    public string RoomPopup()
    {
        string innerHtml = roomRoot.GetAttribute("innerHTML");
        if (innerHtml.Contains("popup-container--2rme5 blocking--aSE_8") == false)
            return "";

//         IWebElement container = roomRoot.FindElement(By.ClassName("popup-container--2rme5 blocking--aSE_8"));
//         IWebElement massege = container.FindElement(By.ClassName("content--1YwBT"));

        return "에러팝업";
    }

    public string GetPlayerScore()
    {
        driver.SwitchTo().DefaultContent();
        driver.SwitchTo().Frame(rootFrame);
        return playerRoot.FindElement(By.ClassName("score--ESKhv")).Text;
    }

    public string GetBankerScore()
    {
        driver.SwitchTo().DefaultContent();
        driver.SwitchTo().Frame(rootFrame);
        return bankerRoot.FindElement(By.ClassName("score--ESKhv")).Text;
    }

    public Type_Bet GetRet()
    {
        driver.SwitchTo().DefaultContent();
        driver.SwitchTo().Frame(rootFrame);

        string innerHtmlPlayer = playerRoot.GetAttribute("innerHTML");
        string innerHtmlBanker = bankerRoot.GetAttribute("innerHTML");

        if (innerHtmlPlayer.Contains("win--"))
            return Type_Bet.Player;

        if (innerHtmlBanker.Contains("win--"))
            return Type_Bet.Banker;

        return Type_Bet.Tie;
    }

    public Room_STATE GetRoomState()
    {
        driver.SwitchTo().DefaultContent();
        driver.SwitchTo().Frame(rootFrame);

        if (Global.checkBox_Real == false)
            CheckNoActionStop();
             
        if (chipStackRoot.Displayed)
        {
            return Room_STATE.Room_BetTime;
        }            
        else
        {
            string innerHtmlRoot = bettingRoot.GetAttribute("innerHTML");

            if (innerHtmlRoot.Contains("win--"))
            {
                return Room_STATE.Room_Result;
            }
            else
            {
                return Room_STATE.Room_DealCard;
            }               
        }
    }

    public double GetBetValue()
    {
        driver.SwitchTo().DefaultContent();
        driver.SwitchTo().Frame(rootFrame);

        IWebElement betMoneyInfo;
        string moneyInfo = "";
        try
        {
            betMoneyInfo = bottomLeft.FindElement(By.ClassName("total-bet--3kWGz"));
            //*[@id="root"]/div/div[1]/div/div/div[3]/div[3]/div/div/div[2]
            //*[@id="root"]/div/div[1]/div/div/div[3]/div[3]/div/div/div[2]/span[2]/span[2]
            betMoneyInfo = betMoneyInfo.FindElement(By.XPath(".//span[2]/span[2]"));
            moneyInfo = betMoneyInfo.Text.Trim();
            moneyInfo = moneyInfo.Replace(",", "");
        }
        catch
        {
            return -1.0f;
        }

        if (string.IsNullOrEmpty(moneyInfo))
            return 0.0f;
           
        return double.Parse(moneyInfo);
    }

    public void DoBet(BitArray betCommand_, double betMoney_, bool delay_ = true, bool boubleBet_ = false , int times_ = 1)
    {
        driver.SwitchTo().DefaultContent();
        driver.SwitchTo().Frame(rootFrame);

        if (delay_)
            Thread.Sleep(500);

        double remainMoney = betMoney_;

        foreach (var item in dicChips.Reverse())
        {
            double chipValue = item.Key;
            while (remainMoney >= chipValue)
            {
                string className = item.Value.GetAttribute("class");
                if (className.Contains("disabled"))
                {
                    Global.uiController.PrintSystemLog("금액 부족으로 인한 정지");

                    Global.uiController.metroButton_Stop_Click(null,null);
                    return;
                }

                item.Value.Click();
                remainMoney -= chipValue;
                

                for(int i=0; i< betCommand_.Count; ++i)
                {
                    if (betCommand_.Get(i))
                    {
                        switch(i)
                        {
                            case (int)Type_Bet.Player:
                                if (Global.checkBox_SideBet)
                                    bankerRoot.Click();
                                else
                                    playerRoot.Click();
                                break;
                            case (int)Type_Bet.Banker:
                                if (Global.checkBox_SideBet)
                                    playerRoot.Click();
                                else
                                    bankerRoot.Click();
                                break;
                            case (int)Type_Bet.Tie:                               
                                break;
                        }
                    }
                }
                Thread.Sleep(50);
            }
        }

        if (boubleBet_)
        {
            if (doubleBtn.Enabled)
            {
                for(int i=0; i< times_; ++i)
                {
                    doubleBtn.Click();
                    Thread.Sleep(50);
                }               
            }
            else
            {
                Global.uiController.PrintSystemLog("더블벳 클릭할수 없는 상태");
            }           
        }
                   
    }

    public string GetLastScore()
    {

        IWebElement root = mainRaod.FindElement(By.ClassName("svg--YDulJ"));
        IList<IWebElement> listScore = root.FindElements(By.ClassName("svg--1vtM9"));
        if (listScore.Count > 0)
        {
            string ret = listScore.Last().GetAttribute("name");
            if (ret.Contains("Player"))
                return "Player";
            else if (ret.Contains("Banker"))
                return "Banker";
            else
                return "Tie";
        }
        else
            return "Tie";
    }

    public void PreventionBet()
    {
        driver.SwitchTo().DefaultContent();
        driver.SwitchTo().Frame(rootFrame);
        Thread.Sleep(500);

        IWebElement minChip = dicChips.First().Value;
        minChip.Click();

        playerRoot.Click();
        Thread.Sleep(50);

//         bankerRoot.Click();
//         Thread.Sleep(100);
// 
//         if (returnBtn.Enabled)
//         {
//             returnBtn.Click();
//             Thread.Sleep(100);
//         }
// 
//         if (returnBtn.Enabled)
//         {
//             returnBtn.Click();
//             Thread.Sleep(100);
//         }
    }
}

