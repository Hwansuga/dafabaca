﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using MetroFramework.Controls;
using System.Reflection;
using System.Text.RegularExpressions;
using System.IO;

namespace DafaBaca
{
    public partial class SettingDlg : Form
    {
        List<MetroCheckBox> listCheckBox = new List<MetroCheckBox>();
        List<ItemCondition> listItemCondition = new List<ItemCondition>();

        public SettingDlg()
        {
            InitializeComponent();
            FormClosing += ExitEvent;
        }

        void ExitEvent(object sender, FormClosingEventArgs e)
        {
            if (groupBox_Setting.Visible == false)
                return;

            Global.settingInfo = GetInfoFromUI();
        }

        void SetDefalut()
        {
            comboBox_Rule.SelectedIndex = 0;
            numericUpDown_Num.Value = 4;
            numericUpDown_BaseMoney.Value = 1000;
        }

        private void SettingDlg_Load(object sender, EventArgs e)
        {
            groupBox_Setting.Visible = false;

            CollectController();
            SetDefalut();
        }

        void CollectController()
        {
            List<FieldInfo> listField = GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).ToList();
            List<FieldInfo> listTemp = listField.FindAll(x => x.FieldType == typeof(ItemCondition));
            Util.SrotByNameNymber(listTemp);
            foreach (var item in listTemp)
                listItemCondition.Add((ItemCondition)item.GetValue(this));

            

            listTemp = listField.FindAll(x => x.FieldType == typeof(MetroCheckBox));
            Util.SrotByNameNymber(listTemp);
            foreach (var item in listTemp)
            {
                MetroCheckBox temp = (MetroCheckBox)item.GetValue(this);
                temp.CheckedChanged += metroCheckBox_Enable_Custom_CheckedChanged;
                listCheckBox.Add(temp);
            }                         
        }

        private void metroButton_Save_Click(object sender, EventArgs e)
        {
            string path = Directory.GetCurrentDirectory();
            path += "\\" + "Option";

            if (Directory.Exists(path) == false)
                Directory.CreateDirectory(path);

            SaveFileDialog ofd = new SaveFileDialog();
            ofd.InitialDirectory = path;
            ofd.Title = "설정";
            ofd.Filter = "|*.bofs";

            DialogResult dr = ofd.ShowDialog();

            if (dr == DialogResult.OK)
            {
                string fileName = System.IO.Path.GetFileName(ofd.FileName);
                metroLabel_FileName.Text = fileName;
                List<string> listInfo = GetOptionInfoFromUI();
                Util.SaveTxtFile(fileName.Replace(".bofs" , "") + ".bofs", listInfo , "Option");
                Global.curOptionFile = fileName;
            }
            //취소버튼 클릭시 또는 ESC키로 파일창을 종료 했을경우
            else if (dr == DialogResult.Cancel)
            {
                return;
            }

        }

        public void LoadOptionFromFile(string fileNmae_)
        {
            metroLabel_FileName.Text = fileNmae_;

            List<string> listInfo = new List<string>();
            Util.LoadTxtFile(fileNmae_, ref listInfo, "Option");
            LoadSetting(listInfo);
        }

        private void metroButton_Load_Click(object sender, EventArgs e)
        {
            string path = Directory.GetCurrentDirectory();
            path += "\\" + "Option";

            if (Directory.Exists(path) == false)
                Directory.CreateDirectory(path);

            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = path;
            ofd.Title = "설정";
            ofd.Filter = "|*.bofs";

            DialogResult dr = ofd.ShowDialog();

            if (dr == DialogResult.OK)
            {
                string fileName = ofd.SafeFileName;
               
                LoadOptionFromFile(fileName);
                Global.curOptionFile = fileName;               
            }
            else if (dr == DialogResult.Cancel)
            {
                return;
            }
        }

        private void metroCheckBox_Enable_Custom_CheckedChanged(object sender, EventArgs e)
        {
            MetroCheckBox metroCheckBox = (MetroCheckBox)sender;
            int number = int.Parse(Regex.Replace(metroCheckBox.Name, @"\D", ""));

            listItemCondition[number - 1].Enabled = metroCheckBox.Checked;
        }

        public void LoadSetting(List<string> listInfo_)
        {
            foreach(var item in listInfo_)
            {
                string[] arrInfo = item.Split('/');
                if (arrInfo.Length <= 0)
                    continue;

                switch(arrInfo[0])
                {
                    case "c":
                        {
                            int i = int.Parse(arrInfo[1]);
                            bool enableInfo = bool.Parse(arrInfo[2]);
                            listCheckBox[i].Checked = enableInfo;

                            string[] conditionInfo = arrInfo[3].Split(';');
                            listItemCondition[i].SetConditionUI(conditionInfo);
                        }
                        break;
                    case "r":
                        {
                            int index = int.Parse(arrInfo[1]);
                            comboBox_Rule.SelectedIndex = index;
                        }
                        break;
                    case "n":
                        {
                            int num = int.Parse(arrInfo[1]);
                            numericUpDown_Num.Value = num;
                        }
                        break;
                    case "b":
                        {
                            int index = int.Parse(arrInfo[1]);
                            numericUpDown_BaseMoney.Value = index;
                        }
                        break;
                }
            }

            //Global.settingInfo = GetInfoFromUI();
        }

        public SettingInfo GetInfoFromUI()
        {
            SettingInfo temp = new SettingInfo();

            for (int i = 0; i < listCheckBox.Count; ++i)
            {
                if (listCheckBox[i].Checked == false)
                    continue;

                temp.dicCondition.Add(i, listItemCondition[i].GetListCondition());
            }

            temp.baseMoney = double.Parse(numericUpDown_BaseMoney.Value.ToString());
            temp.startStep = comboBox_Rule.SelectedIndex;
            temp.endStep = int.Parse(numericUpDown_Num.Value.ToString());

            return temp;
        }

        public List<string> GetOptionInfoFromUI()
        {
            List<string> listRet = new List<string>();

            for(int i=0; i< listCheckBox.Count; ++i)
            {
                string temp = "c/" + i + "/" + listCheckBox[i].Checked + "/" + string.Join(";", listItemCondition[i].GetListCondition().ToArray());
                listRet.Add(temp);
            }

            string rule = "r/" + comboBox_Rule.SelectedIndex;
            listRet.Add(rule);

            string num = "n/" + numericUpDown_Num.Value;
            listRet.Add(num);

            string baseMoney = "b/" + numericUpDown_BaseMoney.Value;
            listRet.Add(baseMoney);

            return listRet;
        }

        private void button_Comfirm_Click(object sender, EventArgs e)
        {
            if (textBox_Pw.Text == "009085")
            {
                groupBox_Setting.Visible = true;

                if (string.IsNullOrEmpty(Global.curOptionFile) == false)
                    LoadOptionFromFile(Global.curOptionFile);
            }
                
            else
                groupBox_Setting.Visible = false;
        }
    }
}
