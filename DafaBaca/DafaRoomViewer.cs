﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DafaBaca
{
    public partial class DafaRoomViewer : UserControl
    {
        public DafaRoomViewer()
        {
            InitializeComponent();
        }

        private void DafaRoomViewer_Load(object sender, EventArgs e)
        {

        }

        public void UpdateRoomViewer(DafaRoomController controller_)
        {
            this.Invoke(new MethodInvoker(delegate () {
                metroLabel_RoomName.Text = controller_.roomName;
                metroLabel_KeyValue.Text = controller_.shou + "_" + controller_.round;
                metroLabel_State.Text = Global.ConvertRoomStateToString(controller_.roomState);
                metroLabel_Progress.Text = (BetCommander.GetBetNumber(controller_.betNum) +1) + " - " + (controller_.betStep +1);

                double totalBetMoney = controller_.dicBetMoneyHistory.Values.Sum();
                metroLabel_TotalBetMoney.Text = totalBetMoney.ToString();

                double totalRetMoney = controller_.dicRetMoneyHistory.Values.Sum();
                metroLabel_TotalRetMoney.Text = totalRetMoney.ToString();

                int cntBet = controller_.dicBetHistory.Values.ToList().FindAll(x=>Util.GetIntFromBitArray(x)>0).Count;
                metroLabel_BetCnt.Text = cntBet.ToString();

                metroLabel_CntStepOut.Text = controller_.cntStepOut.ToString();

            }));
        }      
    }
}
