﻿namespace DafaBaca
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.metroButton_LoadSetting = new MetroFramework.Controls.MetroButton();
            this.checkBox_PreventionTimeOut = new System.Windows.Forms.CheckBox();
            this.metroButton_Setting = new MetroFramework.Controls.MetroButton();
            this.metroButton_CreatPage = new MetroFramework.Controls.MetroButton();
            this.metroButton_Start = new MetroFramework.Controls.MetroButton();
            this.metroButton_Stop = new MetroFramework.Controls.MetroButton();
            this.checkBox_Real = new System.Windows.Forms.CheckBox();
            this.checkBox_DoubleBet = new System.Windows.Forms.CheckBox();
            this.numericUpDown_CntDouble = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_TimeDouble = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.metroButton_SaveSetting = new MetroFramework.Controls.MetroButton();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.numericUpDown_Num = new System.Windows.Forms.NumericUpDown();
            this.metroTabControl_System = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPage_System = new MetroFramework.Controls.MetroTabPage();
            this.listBox_LogSystem = new System.Windows.Forms.ListBox();
            this.metroTabPage_Room1 = new MetroFramework.Controls.MetroTabPage();
            this.listBox_LogRoom1 = new System.Windows.Forms.ListBox();
            this.metroTabPage_Room2 = new MetroFramework.Controls.MetroTabPage();
            this.listBox_LogRoom2 = new System.Windows.Forms.ListBox();
            this.metroTabPage_Room3 = new MetroFramework.Controls.MetroTabPage();
            this.listBox_LogRoom3 = new System.Windows.Forms.ListBox();
            this.metroTabPage_Room4 = new MetroFramework.Controls.MetroTabPage();
            this.listBox_LogRoom4 = new System.Windows.Forms.ListBox();
            this.checkBox_Dollar = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label_TotalStepOut = new System.Windows.Forms.Label();
            this.label_TotalBetCnt = new System.Windows.Forms.Label();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.label_TotalRetMoney = new System.Windows.Forms.Label();
            this.tableLayoutPanel_Setting = new System.Windows.Forms.TableLayoutPanel();
            this.checkBox_SideBet = new System.Windows.Forms.CheckBox();
            this.checkBox_CntReTry = new System.Windows.Forms.CheckBox();
            this.numericUpDown_CntReTry = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_RetryDelay = new System.Windows.Forms.NumericUpDown();
            this.checkBox_RetryDelay = new System.Windows.Forms.CheckBox();
            this.checkBox_GoalMoney = new System.Windows.Forms.CheckBox();
            this.checkBox_LimitLoseMoney = new System.Windows.Forms.CheckBox();
            this.checkBox_LmitBetCnt = new System.Windows.Forms.CheckBox();
            this.checkBox_LmitStepoutCnt = new System.Windows.Forms.CheckBox();
            this.checkBox_LimitBetNum = new System.Windows.Forms.CheckBox();
            this.numericUpDown_GoalMoney = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_LimitLoseMoney = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_LmitBetCnt = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_LmitStepoutCnt = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_LimitBetNum = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDown_BaseMoney = new System.Windows.Forms.NumericUpDown();
            this.dafaRoomViewer4 = new DafaBaca.DafaRoomViewer();
            this.dafaRoomViewer3 = new DafaBaca.DafaRoomViewer();
            this.dafaRoomViewer2 = new DafaBaca.DafaRoomViewer();
            this.dafaRoomViewer1 = new DafaBaca.DafaRoomViewer();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_CntDouble)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_TimeDouble)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Num)).BeginInit();
            this.metroTabControl_System.SuspendLayout();
            this.metroTabPage_System.SuspendLayout();
            this.metroTabPage_Room1.SuspendLayout();
            this.metroTabPage_Room2.SuspendLayout();
            this.metroTabPage_Room3.SuspendLayout();
            this.metroTabPage_Room4.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel_Setting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_CntReTry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_RetryDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_GoalMoney)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_LimitLoseMoney)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_LmitBetCnt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_LmitStepoutCnt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_LimitBetNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_BaseMoney)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown_BaseMoney, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.metroButton_LoadSetting, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.checkBox_PreventionTimeOut, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.metroButton_Setting, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.metroButton_CreatPage, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.metroButton_Start, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.metroButton_Stop, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.checkBox_Real, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.checkBox_DoubleBet, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown_CntDouble, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown_TimeDouble, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.metroButton_SaveSetting, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown_Num, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 1, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // metroButton_LoadSetting
            // 
            resources.ApplyResources(this.metroButton_LoadSetting, "metroButton_LoadSetting");
            this.metroButton_LoadSetting.Name = "metroButton_LoadSetting";
            this.metroButton_LoadSetting.Click += new System.EventHandler(this.metroButton_LoadSetting_Click);
            // 
            // checkBox_PreventionTimeOut
            // 
            resources.ApplyResources(this.checkBox_PreventionTimeOut, "checkBox_PreventionTimeOut");
            this.checkBox_PreventionTimeOut.Name = "checkBox_PreventionTimeOut";
            this.checkBox_PreventionTimeOut.UseVisualStyleBackColor = true;
            // 
            // metroButton_Setting
            // 
            resources.ApplyResources(this.metroButton_Setting, "metroButton_Setting");
            this.metroButton_Setting.Name = "metroButton_Setting";
            this.metroButton_Setting.Click += new System.EventHandler(this.metroButton_Setting_Click);
            // 
            // metroButton_CreatPage
            // 
            resources.ApplyResources(this.metroButton_CreatPage, "metroButton_CreatPage");
            this.metroButton_CreatPage.Name = "metroButton_CreatPage";
            this.metroButton_CreatPage.Click += new System.EventHandler(this.metroButton_CreatPage_Click);
            // 
            // metroButton_Start
            // 
            resources.ApplyResources(this.metroButton_Start, "metroButton_Start");
            this.metroButton_Start.Name = "metroButton_Start";
            this.metroButton_Start.Click += new System.EventHandler(this.metroButton_Start_Click);
            // 
            // metroButton_Stop
            // 
            resources.ApplyResources(this.metroButton_Stop, "metroButton_Stop");
            this.metroButton_Stop.Name = "metroButton_Stop";
            this.metroButton_Stop.Click += new System.EventHandler(this.metroButton_Stop_Click);
            // 
            // checkBox_Real
            // 
            resources.ApplyResources(this.checkBox_Real, "checkBox_Real");
            this.checkBox_Real.Name = "checkBox_Real";
            this.checkBox_Real.UseVisualStyleBackColor = true;
            // 
            // checkBox_DoubleBet
            // 
            resources.ApplyResources(this.checkBox_DoubleBet, "checkBox_DoubleBet");
            this.checkBox_DoubleBet.Name = "checkBox_DoubleBet";
            this.checkBox_DoubleBet.UseVisualStyleBackColor = true;
            this.checkBox_DoubleBet.CheckedChanged += new System.EventHandler(this.checkBox_DoubleBet_CheckedChanged);
            // 
            // numericUpDown_CntDouble
            // 
            resources.ApplyResources(this.numericUpDown_CntDouble, "numericUpDown_CntDouble");
            this.numericUpDown_CntDouble.Maximum = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.numericUpDown_CntDouble.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDown_CntDouble.Name = "numericUpDown_CntDouble";
            this.numericUpDown_CntDouble.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // numericUpDown_TimeDouble
            // 
            resources.ApplyResources(this.numericUpDown_TimeDouble, "numericUpDown_TimeDouble");
            this.numericUpDown_TimeDouble.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDown_TimeDouble.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_TimeDouble.Name = "numericUpDown_TimeDouble";
            this.numericUpDown_TimeDouble.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // metroButton_SaveSetting
            // 
            resources.ApplyResources(this.metroButton_SaveSetting, "metroButton_SaveSetting");
            this.metroButton_SaveSetting.Name = "metroButton_SaveSetting";
            this.metroButton_SaveSetting.Click += new System.EventHandler(this.metroButton_SaveSetting_Click);
            // 
            // metroLabel2
            // 
            resources.ApplyResources(this.metroLabel2, "metroLabel2");
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // numericUpDown_Num
            // 
            resources.ApplyResources(this.numericUpDown_Num, "numericUpDown_Num");
            this.numericUpDown_Num.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown_Num.Minimum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericUpDown_Num.Name = "numericUpDown_Num";
            this.numericUpDown_Num.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericUpDown_Num.ValueChanged += new System.EventHandler(this.numericUpDown_Num_ValueChanged);
            // 
            // metroTabControl_System
            // 
            this.metroTabControl_System.Controls.Add(this.metroTabPage_System);
            this.metroTabControl_System.Controls.Add(this.metroTabPage_Room1);
            this.metroTabControl_System.Controls.Add(this.metroTabPage_Room2);
            this.metroTabControl_System.Controls.Add(this.metroTabPage_Room3);
            this.metroTabControl_System.Controls.Add(this.metroTabPage_Room4);
            resources.ApplyResources(this.metroTabControl_System, "metroTabControl_System");
            this.metroTabControl_System.FontWeight = MetroFramework.MetroTabControlWeight.Bold;
            this.metroTabControl_System.Name = "metroTabControl_System";
            this.metroTabControl_System.SelectedIndex = 4;
            this.metroTabControl_System.Style = MetroFramework.MetroColorStyle.Black;
            this.metroTabControl_System.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTabControl_System.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTabControl_System.UseStyleColors = true;
            // 
            // metroTabPage_System
            // 
            this.metroTabPage_System.Controls.Add(this.listBox_LogSystem);
            this.metroTabPage_System.HorizontalScrollbarBarColor = true;
            resources.ApplyResources(this.metroTabPage_System, "metroTabPage_System");
            this.metroTabPage_System.Name = "metroTabPage_System";
            this.metroTabPage_System.VerticalScrollbarBarColor = true;
            // 
            // listBox_LogSystem
            // 
            resources.ApplyResources(this.listBox_LogSystem, "listBox_LogSystem");
            this.listBox_LogSystem.FormattingEnabled = true;
            this.listBox_LogSystem.Name = "listBox_LogSystem";
            // 
            // metroTabPage_Room1
            // 
            this.metroTabPage_Room1.Controls.Add(this.listBox_LogRoom1);
            this.metroTabPage_Room1.HorizontalScrollbarBarColor = true;
            resources.ApplyResources(this.metroTabPage_Room1, "metroTabPage_Room1");
            this.metroTabPage_Room1.Name = "metroTabPage_Room1";
            this.metroTabPage_Room1.VerticalScrollbarBarColor = true;
            // 
            // listBox_LogRoom1
            // 
            resources.ApplyResources(this.listBox_LogRoom1, "listBox_LogRoom1");
            this.listBox_LogRoom1.FormattingEnabled = true;
            this.listBox_LogRoom1.Name = "listBox_LogRoom1";
            // 
            // metroTabPage_Room2
            // 
            this.metroTabPage_Room2.Controls.Add(this.listBox_LogRoom2);
            this.metroTabPage_Room2.HorizontalScrollbarBarColor = true;
            resources.ApplyResources(this.metroTabPage_Room2, "metroTabPage_Room2");
            this.metroTabPage_Room2.Name = "metroTabPage_Room2";
            this.metroTabPage_Room2.VerticalScrollbarBarColor = true;
            // 
            // listBox_LogRoom2
            // 
            resources.ApplyResources(this.listBox_LogRoom2, "listBox_LogRoom2");
            this.listBox_LogRoom2.FormattingEnabled = true;
            this.listBox_LogRoom2.Name = "listBox_LogRoom2";
            // 
            // metroTabPage_Room3
            // 
            this.metroTabPage_Room3.Controls.Add(this.listBox_LogRoom3);
            this.metroTabPage_Room3.HorizontalScrollbarBarColor = true;
            resources.ApplyResources(this.metroTabPage_Room3, "metroTabPage_Room3");
            this.metroTabPage_Room3.Name = "metroTabPage_Room3";
            this.metroTabPage_Room3.VerticalScrollbarBarColor = true;
            // 
            // listBox_LogRoom3
            // 
            resources.ApplyResources(this.listBox_LogRoom3, "listBox_LogRoom3");
            this.listBox_LogRoom3.FormattingEnabled = true;
            this.listBox_LogRoom3.Name = "listBox_LogRoom3";
            // 
            // metroTabPage_Room4
            // 
            this.metroTabPage_Room4.Controls.Add(this.listBox_LogRoom4);
            this.metroTabPage_Room4.HorizontalScrollbarBarColor = true;
            resources.ApplyResources(this.metroTabPage_Room4, "metroTabPage_Room4");
            this.metroTabPage_Room4.Name = "metroTabPage_Room4";
            this.metroTabPage_Room4.VerticalScrollbarBarColor = true;
            // 
            // listBox_LogRoom4
            // 
            resources.ApplyResources(this.listBox_LogRoom4, "listBox_LogRoom4");
            this.listBox_LogRoom4.FormattingEnabled = true;
            this.listBox_LogRoom4.Name = "listBox_LogRoom4";
            // 
            // checkBox_Dollar
            // 
            resources.ApplyResources(this.checkBox_Dollar, "checkBox_Dollar");
            this.checkBox_Dollar.Name = "checkBox_Dollar";
            this.checkBox_Dollar.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            resources.ApplyResources(this.tableLayoutPanel2, "tableLayoutPanel2");
            this.tableLayoutPanel2.Controls.Add(this.label_TotalStepOut, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.label_TotalBetCnt, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel4, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.metroLabel5, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label_TotalRetMoney, 0, 1);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            // 
            // label_TotalStepOut
            // 
            resources.ApplyResources(this.label_TotalStepOut, "label_TotalStepOut");
            this.label_TotalStepOut.Name = "label_TotalStepOut";
            // 
            // label_TotalBetCnt
            // 
            resources.ApplyResources(this.label_TotalBetCnt, "label_TotalBetCnt");
            this.label_TotalBetCnt.Name = "label_TotalBetCnt";
            // 
            // metroLabel1
            // 
            resources.ApplyResources(this.metroLabel1, "metroLabel1");
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // metroLabel4
            // 
            resources.ApplyResources(this.metroLabel4, "metroLabel4");
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // metroLabel5
            // 
            resources.ApplyResources(this.metroLabel5, "metroLabel5");
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // label_TotalRetMoney
            // 
            resources.ApplyResources(this.label_TotalRetMoney, "label_TotalRetMoney");
            this.label_TotalRetMoney.Name = "label_TotalRetMoney";
            // 
            // tableLayoutPanel_Setting
            // 
            resources.ApplyResources(this.tableLayoutPanel_Setting, "tableLayoutPanel_Setting");
            this.tableLayoutPanel_Setting.Controls.Add(this.checkBox_SideBet, 0, 2);
            this.tableLayoutPanel_Setting.Controls.Add(this.checkBox_CntReTry, 0, 0);
            this.tableLayoutPanel_Setting.Controls.Add(this.numericUpDown_CntReTry, 1, 0);
            this.tableLayoutPanel_Setting.Controls.Add(this.numericUpDown_RetryDelay, 1, 1);
            this.tableLayoutPanel_Setting.Controls.Add(this.checkBox_RetryDelay, 0, 1);
            this.tableLayoutPanel_Setting.Controls.Add(this.checkBox_GoalMoney, 2, 0);
            this.tableLayoutPanel_Setting.Controls.Add(this.checkBox_LimitLoseMoney, 2, 1);
            this.tableLayoutPanel_Setting.Controls.Add(this.checkBox_LmitBetCnt, 4, 0);
            this.tableLayoutPanel_Setting.Controls.Add(this.checkBox_LmitStepoutCnt, 4, 1);
            this.tableLayoutPanel_Setting.Controls.Add(this.checkBox_LimitBetNum, 4, 2);
            this.tableLayoutPanel_Setting.Controls.Add(this.numericUpDown_GoalMoney, 3, 0);
            this.tableLayoutPanel_Setting.Controls.Add(this.numericUpDown_LimitLoseMoney, 3, 1);
            this.tableLayoutPanel_Setting.Controls.Add(this.numericUpDown_LmitBetCnt, 5, 0);
            this.tableLayoutPanel_Setting.Controls.Add(this.numericUpDown_LmitStepoutCnt, 5, 1);
            this.tableLayoutPanel_Setting.Controls.Add(this.numericUpDown_LimitBetNum, 5, 2);
            this.tableLayoutPanel_Setting.Name = "tableLayoutPanel_Setting";
            // 
            // checkBox_SideBet
            // 
            resources.ApplyResources(this.checkBox_SideBet, "checkBox_SideBet");
            this.checkBox_SideBet.Name = "checkBox_SideBet";
            this.checkBox_SideBet.UseVisualStyleBackColor = true;
            // 
            // checkBox_CntReTry
            // 
            resources.ApplyResources(this.checkBox_CntReTry, "checkBox_CntReTry");
            this.checkBox_CntReTry.Name = "checkBox_CntReTry";
            this.checkBox_CntReTry.UseVisualStyleBackColor = true;
            this.checkBox_CntReTry.CheckedChanged += new System.EventHandler(this.checkBox_CntReTry_CheckedChanged);
            // 
            // numericUpDown_CntReTry
            // 
            resources.ApplyResources(this.numericUpDown_CntReTry, "numericUpDown_CntReTry");
            this.numericUpDown_CntReTry.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_CntReTry.Name = "numericUpDown_CntReTry";
            this.numericUpDown_CntReTry.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericUpDown_RetryDelay
            // 
            resources.ApplyResources(this.numericUpDown_RetryDelay, "numericUpDown_RetryDelay");
            this.numericUpDown_RetryDelay.Maximum = new decimal(new int[] {
            60000,
            0,
            0,
            0});
            this.numericUpDown_RetryDelay.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_RetryDelay.Name = "numericUpDown_RetryDelay";
            this.numericUpDown_RetryDelay.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // checkBox_RetryDelay
            // 
            resources.ApplyResources(this.checkBox_RetryDelay, "checkBox_RetryDelay");
            this.checkBox_RetryDelay.Name = "checkBox_RetryDelay";
            this.checkBox_RetryDelay.UseVisualStyleBackColor = true;
            this.checkBox_RetryDelay.CheckedChanged += new System.EventHandler(this.checkBox_RetryDelay_CheckedChanged);
            // 
            // checkBox_GoalMoney
            // 
            resources.ApplyResources(this.checkBox_GoalMoney, "checkBox_GoalMoney");
            this.checkBox_GoalMoney.Name = "checkBox_GoalMoney";
            this.checkBox_GoalMoney.UseVisualStyleBackColor = true;
            this.checkBox_GoalMoney.CheckedChanged += new System.EventHandler(this.checkBox_GoalMoney_CheckedChanged);
            // 
            // checkBox_LimitLoseMoney
            // 
            resources.ApplyResources(this.checkBox_LimitLoseMoney, "checkBox_LimitLoseMoney");
            this.checkBox_LimitLoseMoney.Name = "checkBox_LimitLoseMoney";
            this.checkBox_LimitLoseMoney.UseVisualStyleBackColor = true;
            this.checkBox_LimitLoseMoney.CheckedChanged += new System.EventHandler(this.checkBox_LimitLoseMoney_CheckedChanged);
            // 
            // checkBox_LmitBetCnt
            // 
            resources.ApplyResources(this.checkBox_LmitBetCnt, "checkBox_LmitBetCnt");
            this.checkBox_LmitBetCnt.Name = "checkBox_LmitBetCnt";
            this.checkBox_LmitBetCnt.UseVisualStyleBackColor = true;
            this.checkBox_LmitBetCnt.CheckedChanged += new System.EventHandler(this.checkBox_LmitBetCnt_CheckedChanged);
            // 
            // checkBox_LmitStepoutCnt
            // 
            resources.ApplyResources(this.checkBox_LmitStepoutCnt, "checkBox_LmitStepoutCnt");
            this.checkBox_LmitStepoutCnt.Name = "checkBox_LmitStepoutCnt";
            this.checkBox_LmitStepoutCnt.UseVisualStyleBackColor = true;
            this.checkBox_LmitStepoutCnt.CheckedChanged += new System.EventHandler(this.checkBox_LmitStepoutCnt_CheckedChanged);
            // 
            // checkBox_LimitBetNum
            // 
            resources.ApplyResources(this.checkBox_LimitBetNum, "checkBox_LimitBetNum");
            this.checkBox_LimitBetNum.Name = "checkBox_LimitBetNum";
            this.checkBox_LimitBetNum.UseVisualStyleBackColor = true;
            this.checkBox_LimitBetNum.CheckedChanged += new System.EventHandler(this.checkBox_LimitBetNum_CheckedChanged);
            // 
            // numericUpDown_GoalMoney
            // 
            resources.ApplyResources(this.numericUpDown_GoalMoney, "numericUpDown_GoalMoney");
            this.numericUpDown_GoalMoney.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.numericUpDown_GoalMoney.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown_GoalMoney.Name = "numericUpDown_GoalMoney";
            this.numericUpDown_GoalMoney.Value = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            // 
            // numericUpDown_LimitLoseMoney
            // 
            resources.ApplyResources(this.numericUpDown_LimitLoseMoney, "numericUpDown_LimitLoseMoney");
            this.numericUpDown_LimitLoseMoney.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.numericUpDown_LimitLoseMoney.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown_LimitLoseMoney.Name = "numericUpDown_LimitLoseMoney";
            this.numericUpDown_LimitLoseMoney.Value = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            // 
            // numericUpDown_LmitBetCnt
            // 
            resources.ApplyResources(this.numericUpDown_LmitBetCnt, "numericUpDown_LmitBetCnt");
            this.numericUpDown_LmitBetCnt.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numericUpDown_LmitBetCnt.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numericUpDown_LmitBetCnt.Name = "numericUpDown_LmitBetCnt";
            this.numericUpDown_LmitBetCnt.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // numericUpDown_LmitStepoutCnt
            // 
            resources.ApplyResources(this.numericUpDown_LmitStepoutCnt, "numericUpDown_LmitStepoutCnt");
            this.numericUpDown_LmitStepoutCnt.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.numericUpDown_LmitStepoutCnt.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_LmitStepoutCnt.Name = "numericUpDown_LmitStepoutCnt";
            this.numericUpDown_LmitStepoutCnt.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericUpDown_LimitBetNum
            // 
            resources.ApplyResources(this.numericUpDown_LimitBetNum, "numericUpDown_LimitBetNum");
            this.numericUpDown_LimitBetNum.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown_LimitBetNum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_LimitBetNum.Name = "numericUpDown_LimitBetNum";
            this.numericUpDown_LimitBetNum.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // numericUpDown_BaseMoney
            // 
            resources.ApplyResources(this.numericUpDown_BaseMoney, "numericUpDown_BaseMoney");
            this.numericUpDown_BaseMoney.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_BaseMoney.Maximum = new decimal(new int[] {
            5000000,
            0,
            0,
            0});
            this.numericUpDown_BaseMoney.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_BaseMoney.Name = "numericUpDown_BaseMoney";
            this.numericUpDown_BaseMoney.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // dafaRoomViewer4
            // 
            resources.ApplyResources(this.dafaRoomViewer4, "dafaRoomViewer4");
            this.dafaRoomViewer4.Name = "dafaRoomViewer4";
            // 
            // dafaRoomViewer3
            // 
            resources.ApplyResources(this.dafaRoomViewer3, "dafaRoomViewer3");
            this.dafaRoomViewer3.Name = "dafaRoomViewer3";
            // 
            // dafaRoomViewer2
            // 
            resources.ApplyResources(this.dafaRoomViewer2, "dafaRoomViewer2");
            this.dafaRoomViewer2.Name = "dafaRoomViewer2";
            // 
            // dafaRoomViewer1
            // 
            resources.ApplyResources(this.dafaRoomViewer1, "dafaRoomViewer1");
            this.dafaRoomViewer1.Name = "dafaRoomViewer1";
            // 
            // Form1
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Controls.Add(this.tableLayoutPanel_Setting);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.dafaRoomViewer4);
            this.Controls.Add(this.dafaRoomViewer3);
            this.Controls.Add(this.dafaRoomViewer2);
            this.Controls.Add(this.dafaRoomViewer1);
            this.Controls.Add(this.metroTabControl_System);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.checkBox_Dollar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.HelpButton = true;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_CntDouble)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_TimeDouble)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Num)).EndInit();
            this.metroTabControl_System.ResumeLayout(false);
            this.metroTabPage_System.ResumeLayout(false);
            this.metroTabPage_Room1.ResumeLayout(false);
            this.metroTabPage_Room2.ResumeLayout(false);
            this.metroTabPage_Room3.ResumeLayout(false);
            this.metroTabPage_Room4.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel_Setting.ResumeLayout(false);
            this.tableLayoutPanel_Setting.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_CntReTry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_RetryDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_GoalMoney)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_LimitLoseMoney)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_LmitBetCnt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_LmitStepoutCnt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_LimitBetNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_BaseMoney)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroButton metroButton_Start;
        private MetroFramework.Controls.MetroButton metroButton_Stop;
        private MetroFramework.Controls.MetroTabControl metroTabControl_System;
        private MetroFramework.Controls.MetroTabPage metroTabPage_System;
        private MetroFramework.Controls.MetroTabPage metroTabPage_Room1;
        private MetroFramework.Controls.MetroTabPage metroTabPage_Room2;
        private MetroFramework.Controls.MetroTabPage metroTabPage_Room3;
        private MetroFramework.Controls.MetroTabPage metroTabPage_Room4;
        private System.Windows.Forms.ListBox listBox_LogSystem;
        private System.Windows.Forms.ListBox listBox_LogRoom1;
        private System.Windows.Forms.ListBox listBox_LogRoom2;
        private System.Windows.Forms.ListBox listBox_LogRoom3;
        private System.Windows.Forms.ListBox listBox_LogRoom4;
        private MetroFramework.Controls.MetroButton metroButton_CreatPage;
        private MetroFramework.Controls.MetroButton metroButton_Setting;
        private System.Windows.Forms.CheckBox checkBox_Real;
        private System.Windows.Forms.CheckBox checkBox_PreventionTimeOut;
        private DafaRoomViewer dafaRoomViewer1;
        private DafaRoomViewer dafaRoomViewer2;
        private DafaRoomViewer dafaRoomViewer3;
        private DafaRoomViewer dafaRoomViewer4;
        private System.Windows.Forms.CheckBox checkBox_DoubleBet;
        private System.Windows.Forms.NumericUpDown numericUpDown_CntDouble;
        private System.Windows.Forms.NumericUpDown numericUpDown_TimeDouble;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBox_Dollar;
        private MetroFramework.Controls.MetroButton metroButton_LoadSetting;
        private MetroFramework.Controls.MetroButton metroButton_SaveSetting;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private System.Windows.Forms.NumericUpDown numericUpDown_Num;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private System.Windows.Forms.Label label_TotalStepOut;
        private System.Windows.Forms.Label label_TotalBetCnt;
        private System.Windows.Forms.Label label_TotalRetMoney;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_Setting;
        private System.Windows.Forms.NumericUpDown numericUpDown_CntReTry;
        private System.Windows.Forms.NumericUpDown numericUpDown_RetryDelay;
        private System.Windows.Forms.CheckBox checkBox_RetryDelay;
        private System.Windows.Forms.CheckBox checkBox_GoalMoney;
        private System.Windows.Forms.CheckBox checkBox_LimitLoseMoney;
        private System.Windows.Forms.CheckBox checkBox_LimitBetNum;
        private System.Windows.Forms.CheckBox checkBox_LmitBetCnt;
        private System.Windows.Forms.CheckBox checkBox_LmitStepoutCnt;
        private System.Windows.Forms.NumericUpDown numericUpDown_GoalMoney;
        private System.Windows.Forms.NumericUpDown numericUpDown_LimitLoseMoney;
        private System.Windows.Forms.NumericUpDown numericUpDown_LmitBetCnt;
        private System.Windows.Forms.NumericUpDown numericUpDown_LmitStepoutCnt;
        private System.Windows.Forms.NumericUpDown numericUpDown_LimitBetNum;
        private System.Windows.Forms.CheckBox checkBox_CntReTry;
        private System.Windows.Forms.CheckBox checkBox_SideBet;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.NumericUpDown numericUpDown_BaseMoney;
    }
}

