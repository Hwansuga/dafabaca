﻿namespace DafaBaca
{
    partial class SettingDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingDlg));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel_FileName = new MetroFramework.Controls.MetroLabel();
            this.metroButton_Save = new MetroFramework.Controls.MetroButton();
            this.metroButton_Load = new MetroFramework.Controls.MetroButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.numericUpDown_Num = new System.Windows.Forms.NumericUpDown();
            this.comboBox_Rule = new System.Windows.Forms.ComboBox();
            this.numericUpDown_BaseMoney = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanel_Condition = new System.Windows.Forms.TableLayoutPanel();
            this.metroCheckBox_Enable10 = new MetroFramework.Controls.MetroCheckBox();
            this.metroCheckBox_Enable9 = new MetroFramework.Controls.MetroCheckBox();
            this.metroCheckBox_Enable8 = new MetroFramework.Controls.MetroCheckBox();
            this.metroCheckBox_Enable7 = new MetroFramework.Controls.MetroCheckBox();
            this.metroCheckBox_Enable6 = new MetroFramework.Controls.MetroCheckBox();
            this.metroCheckBox_Enable5 = new MetroFramework.Controls.MetroCheckBox();
            this.metroCheckBox_Enable4 = new MetroFramework.Controls.MetroCheckBox();
            this.metroCheckBox_Enable3 = new MetroFramework.Controls.MetroCheckBox();
            this.metroCheckBox_Enable2 = new MetroFramework.Controls.MetroCheckBox();
            this.metroCheckBox_Enable1 = new MetroFramework.Controls.MetroCheckBox();
            this.groupBox_Setting = new System.Windows.Forms.GroupBox();
            this.textBox_Pw = new System.Windows.Forms.TextBox();
            this.button_Comfirm = new System.Windows.Forms.Button();
            this.itemCondition1 = new DafaBaca.ItemCondition();
            this.itemCondition2 = new DafaBaca.ItemCondition();
            this.itemCondition3 = new DafaBaca.ItemCondition();
            this.itemCondition4 = new DafaBaca.ItemCondition();
            this.itemCondition5 = new DafaBaca.ItemCondition();
            this.itemCondition6 = new DafaBaca.ItemCondition();
            this.itemCondition7 = new DafaBaca.ItemCondition();
            this.itemCondition8 = new DafaBaca.ItemCondition();
            this.itemCondition9 = new DafaBaca.ItemCondition();
            this.itemCondition10 = new DafaBaca.ItemCondition();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Num)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_BaseMoney)).BeginInit();
            this.tableLayoutPanel_Condition.SuspendLayout();
            this.groupBox_Setting.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this.metroLabel3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel_FileName, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.metroButton_Save, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.metroButton_Load, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown_Num, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.comboBox_Rule, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown_BaseMoney, 1, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // metroLabel3
            // 
            resources.ApplyResources(this.metroLabel3, "metroLabel3");
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // metroLabel_FileName
            // 
            resources.ApplyResources(this.metroLabel_FileName, "metroLabel_FileName");
            this.metroLabel_FileName.Name = "metroLabel_FileName";
            this.metroLabel_FileName.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // metroButton_Save
            // 
            resources.ApplyResources(this.metroButton_Save, "metroButton_Save");
            this.metroButton_Save.Name = "metroButton_Save";
            this.metroButton_Save.Click += new System.EventHandler(this.metroButton_Save_Click);
            // 
            // metroButton_Load
            // 
            resources.ApplyResources(this.metroButton_Load, "metroButton_Load");
            this.metroButton_Load.Name = "metroButton_Load";
            this.metroButton_Load.Click += new System.EventHandler(this.metroButton_Load_Click);
            // 
            // metroLabel1
            // 
            resources.ApplyResources(this.metroLabel1, "metroLabel1");
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // metroLabel2
            // 
            resources.ApplyResources(this.metroLabel2, "metroLabel2");
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // numericUpDown_Num
            // 
            resources.ApplyResources(this.numericUpDown_Num, "numericUpDown_Num");
            this.numericUpDown_Num.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown_Num.Minimum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericUpDown_Num.Name = "numericUpDown_Num";
            this.numericUpDown_Num.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // comboBox_Rule
            // 
            this.comboBox_Rule.FormattingEnabled = true;
            this.comboBox_Rule.Items.AddRange(new object[] {
            resources.GetString("comboBox_Rule.Items"),
            resources.GetString("comboBox_Rule.Items1"),
            resources.GetString("comboBox_Rule.Items2"),
            resources.GetString("comboBox_Rule.Items3"),
            resources.GetString("comboBox_Rule.Items4"),
            resources.GetString("comboBox_Rule.Items5"),
            resources.GetString("comboBox_Rule.Items6"),
            resources.GetString("comboBox_Rule.Items7"),
            resources.GetString("comboBox_Rule.Items8")});
            resources.ApplyResources(this.comboBox_Rule, "comboBox_Rule");
            this.comboBox_Rule.Name = "comboBox_Rule";
            // 
            // numericUpDown_BaseMoney
            // 
            this.numericUpDown_BaseMoney.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            resources.ApplyResources(this.numericUpDown_BaseMoney, "numericUpDown_BaseMoney");
            this.numericUpDown_BaseMoney.Maximum = new decimal(new int[] {
            5000000,
            0,
            0,
            0});
            this.numericUpDown_BaseMoney.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_BaseMoney.Name = "numericUpDown_BaseMoney";
            this.numericUpDown_BaseMoney.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // tableLayoutPanel_Condition
            // 
            resources.ApplyResources(this.tableLayoutPanel_Condition, "tableLayoutPanel_Condition");
            this.tableLayoutPanel_Condition.Controls.Add(this.metroCheckBox_Enable10, 0, 9);
            this.tableLayoutPanel_Condition.Controls.Add(this.metroCheckBox_Enable9, 0, 8);
            this.tableLayoutPanel_Condition.Controls.Add(this.metroCheckBox_Enable8, 0, 7);
            this.tableLayoutPanel_Condition.Controls.Add(this.metroCheckBox_Enable7, 0, 6);
            this.tableLayoutPanel_Condition.Controls.Add(this.metroCheckBox_Enable6, 0, 5);
            this.tableLayoutPanel_Condition.Controls.Add(this.metroCheckBox_Enable5, 0, 4);
            this.tableLayoutPanel_Condition.Controls.Add(this.metroCheckBox_Enable4, 0, 3);
            this.tableLayoutPanel_Condition.Controls.Add(this.metroCheckBox_Enable3, 0, 2);
            this.tableLayoutPanel_Condition.Controls.Add(this.metroCheckBox_Enable2, 0, 1);
            this.tableLayoutPanel_Condition.Controls.Add(this.itemCondition1, 1, 0);
            this.tableLayoutPanel_Condition.Controls.Add(this.metroCheckBox_Enable1, 0, 0);
            this.tableLayoutPanel_Condition.Controls.Add(this.itemCondition2, 1, 1);
            this.tableLayoutPanel_Condition.Controls.Add(this.itemCondition3, 1, 2);
            this.tableLayoutPanel_Condition.Controls.Add(this.itemCondition4, 1, 3);
            this.tableLayoutPanel_Condition.Controls.Add(this.itemCondition5, 1, 4);
            this.tableLayoutPanel_Condition.Controls.Add(this.itemCondition6, 1, 5);
            this.tableLayoutPanel_Condition.Controls.Add(this.itemCondition7, 1, 6);
            this.tableLayoutPanel_Condition.Controls.Add(this.itemCondition8, 1, 7);
            this.tableLayoutPanel_Condition.Controls.Add(this.itemCondition9, 1, 8);
            this.tableLayoutPanel_Condition.Controls.Add(this.itemCondition10, 1, 9);
            this.tableLayoutPanel_Condition.Name = "tableLayoutPanel_Condition";
            // 
            // metroCheckBox_Enable10
            // 
            resources.ApplyResources(this.metroCheckBox_Enable10, "metroCheckBox_Enable10");
            this.metroCheckBox_Enable10.Checked = true;
            this.metroCheckBox_Enable10.CheckState = System.Windows.Forms.CheckState.Checked;
            this.metroCheckBox_Enable10.Name = "metroCheckBox_Enable10";
            this.metroCheckBox_Enable10.UseVisualStyleBackColor = true;
            // 
            // metroCheckBox_Enable9
            // 
            resources.ApplyResources(this.metroCheckBox_Enable9, "metroCheckBox_Enable9");
            this.metroCheckBox_Enable9.Checked = true;
            this.metroCheckBox_Enable9.CheckState = System.Windows.Forms.CheckState.Checked;
            this.metroCheckBox_Enable9.Name = "metroCheckBox_Enable9";
            this.metroCheckBox_Enable9.UseVisualStyleBackColor = true;
            // 
            // metroCheckBox_Enable8
            // 
            resources.ApplyResources(this.metroCheckBox_Enable8, "metroCheckBox_Enable8");
            this.metroCheckBox_Enable8.Checked = true;
            this.metroCheckBox_Enable8.CheckState = System.Windows.Forms.CheckState.Checked;
            this.metroCheckBox_Enable8.Name = "metroCheckBox_Enable8";
            this.metroCheckBox_Enable8.UseVisualStyleBackColor = true;
            // 
            // metroCheckBox_Enable7
            // 
            resources.ApplyResources(this.metroCheckBox_Enable7, "metroCheckBox_Enable7");
            this.metroCheckBox_Enable7.Checked = true;
            this.metroCheckBox_Enable7.CheckState = System.Windows.Forms.CheckState.Checked;
            this.metroCheckBox_Enable7.Name = "metroCheckBox_Enable7";
            this.metroCheckBox_Enable7.UseVisualStyleBackColor = true;
            // 
            // metroCheckBox_Enable6
            // 
            resources.ApplyResources(this.metroCheckBox_Enable6, "metroCheckBox_Enable6");
            this.metroCheckBox_Enable6.Checked = true;
            this.metroCheckBox_Enable6.CheckState = System.Windows.Forms.CheckState.Checked;
            this.metroCheckBox_Enable6.Name = "metroCheckBox_Enable6";
            this.metroCheckBox_Enable6.UseVisualStyleBackColor = true;
            // 
            // metroCheckBox_Enable5
            // 
            resources.ApplyResources(this.metroCheckBox_Enable5, "metroCheckBox_Enable5");
            this.metroCheckBox_Enable5.Checked = true;
            this.metroCheckBox_Enable5.CheckState = System.Windows.Forms.CheckState.Checked;
            this.metroCheckBox_Enable5.Name = "metroCheckBox_Enable5";
            this.metroCheckBox_Enable5.UseVisualStyleBackColor = true;
            // 
            // metroCheckBox_Enable4
            // 
            resources.ApplyResources(this.metroCheckBox_Enable4, "metroCheckBox_Enable4");
            this.metroCheckBox_Enable4.Checked = true;
            this.metroCheckBox_Enable4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.metroCheckBox_Enable4.Name = "metroCheckBox_Enable4";
            this.metroCheckBox_Enable4.UseVisualStyleBackColor = true;
            // 
            // metroCheckBox_Enable3
            // 
            resources.ApplyResources(this.metroCheckBox_Enable3, "metroCheckBox_Enable3");
            this.metroCheckBox_Enable3.Checked = true;
            this.metroCheckBox_Enable3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.metroCheckBox_Enable3.Name = "metroCheckBox_Enable3";
            this.metroCheckBox_Enable3.UseVisualStyleBackColor = true;
            // 
            // metroCheckBox_Enable2
            // 
            resources.ApplyResources(this.metroCheckBox_Enable2, "metroCheckBox_Enable2");
            this.metroCheckBox_Enable2.Checked = true;
            this.metroCheckBox_Enable2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.metroCheckBox_Enable2.Name = "metroCheckBox_Enable2";
            this.metroCheckBox_Enable2.UseVisualStyleBackColor = true;
            // 
            // metroCheckBox_Enable1
            // 
            resources.ApplyResources(this.metroCheckBox_Enable1, "metroCheckBox_Enable1");
            this.metroCheckBox_Enable1.Checked = true;
            this.metroCheckBox_Enable1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.metroCheckBox_Enable1.Name = "metroCheckBox_Enable1";
            this.metroCheckBox_Enable1.UseVisualStyleBackColor = true;
            // 
            // groupBox_Setting
            // 
            resources.ApplyResources(this.groupBox_Setting, "groupBox_Setting");
            this.groupBox_Setting.Controls.Add(this.tableLayoutPanel1);
            this.groupBox_Setting.Controls.Add(this.tableLayoutPanel_Condition);
            this.groupBox_Setting.Name = "groupBox_Setting";
            this.groupBox_Setting.TabStop = false;
            // 
            // textBox_Pw
            // 
            resources.ApplyResources(this.textBox_Pw, "textBox_Pw");
            this.textBox_Pw.Name = "textBox_Pw";
            // 
            // button_Comfirm
            // 
            resources.ApplyResources(this.button_Comfirm, "button_Comfirm");
            this.button_Comfirm.Name = "button_Comfirm";
            this.button_Comfirm.UseVisualStyleBackColor = true;
            this.button_Comfirm.Click += new System.EventHandler(this.button_Comfirm_Click);
            // 
            // itemCondition1
            // 
            resources.ApplyResources(this.itemCondition1, "itemCondition1");
            this.itemCondition1.Name = "itemCondition1";
            // 
            // itemCondition2
            // 
            resources.ApplyResources(this.itemCondition2, "itemCondition2");
            this.itemCondition2.Name = "itemCondition2";
            // 
            // itemCondition3
            // 
            resources.ApplyResources(this.itemCondition3, "itemCondition3");
            this.itemCondition3.Name = "itemCondition3";
            // 
            // itemCondition4
            // 
            resources.ApplyResources(this.itemCondition4, "itemCondition4");
            this.itemCondition4.Name = "itemCondition4";
            // 
            // itemCondition5
            // 
            resources.ApplyResources(this.itemCondition5, "itemCondition5");
            this.itemCondition5.Name = "itemCondition5";
            // 
            // itemCondition6
            // 
            resources.ApplyResources(this.itemCondition6, "itemCondition6");
            this.itemCondition6.Name = "itemCondition6";
            // 
            // itemCondition7
            // 
            resources.ApplyResources(this.itemCondition7, "itemCondition7");
            this.itemCondition7.Name = "itemCondition7";
            // 
            // itemCondition8
            // 
            resources.ApplyResources(this.itemCondition8, "itemCondition8");
            this.itemCondition8.Name = "itemCondition8";
            // 
            // itemCondition9
            // 
            resources.ApplyResources(this.itemCondition9, "itemCondition9");
            this.itemCondition9.Name = "itemCondition9";
            // 
            // itemCondition10
            // 
            resources.ApplyResources(this.itemCondition10, "itemCondition10");
            this.itemCondition10.Name = "itemCondition10";
            // 
            // SettingDlg
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button_Comfirm);
            this.Controls.Add(this.textBox_Pw);
            this.Controls.Add(this.groupBox_Setting);
            this.Name = "SettingDlg";
            this.Load += new System.EventHandler(this.SettingDlg_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Num)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_BaseMoney)).EndInit();
            this.tableLayoutPanel_Condition.ResumeLayout(false);
            this.tableLayoutPanel_Condition.PerformLayout();
            this.groupBox_Setting.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_Condition;
        private ItemCondition itemCondition1;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox_Enable1;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox_Enable10;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox_Enable9;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox_Enable8;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox_Enable7;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox_Enable6;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox_Enable5;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox_Enable4;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox_Enable3;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox_Enable2;
        private ItemCondition itemCondition2;
        private ItemCondition itemCondition3;
        private ItemCondition itemCondition4;
        private ItemCondition itemCondition5;
        private ItemCondition itemCondition6;
        private ItemCondition itemCondition7;
        private ItemCondition itemCondition8;
        private ItemCondition itemCondition9;
        private ItemCondition itemCondition10;
        private MetroFramework.Controls.MetroButton metroButton_Save;
        private MetroFramework.Controls.MetroButton metroButton_Load;
        private MetroFramework.Controls.MetroLabel metroLabel_FileName;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private System.Windows.Forms.NumericUpDown numericUpDown_Num;
        private System.Windows.Forms.ComboBox comboBox_Rule;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private System.Windows.Forms.GroupBox groupBox_Setting;
        private System.Windows.Forms.TextBox textBox_Pw;
        private System.Windows.Forms.Button button_Comfirm;
        public System.Windows.Forms.NumericUpDown numericUpDown_BaseMoney;
    }
}