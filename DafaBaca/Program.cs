﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DafaBaca
{
    static class Program
    {
        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main()
        {
            string mutexKey = "{41829912-8F79-41B6-837E-633877E8E5DF}";

            //check duplication
            if (Util.CheckDuplication(mutexKey))
                return;


            if (Util.CheckExpireDate(new DateTime(2019 ,3,4)))
            {
                MessageBox.Show("기간이 만료되었습니다.");
                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
