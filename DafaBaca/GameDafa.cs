﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DafaBaca
{
    class GameDafa : DriverController
    {
        public string pageMainURL = "https://www.dfbet.com/kr";

        string windowTitle = "Live Casino:";
        public string loginTitle = "다파벳 라이브 딜러에서 라이브 온라인 카지노 게임을 플레이하세요!";

        int driverWitdh = 1280;
        int driverHeight = 900;

        public bool Process_Page()
        {
            CreatDriver();

            driver.Navigate().GoToUrl(pageMainURL);
            driver.Manage().Window.Size = new Size(driverWitdh, driverHeight);
            driver.Manage().Window.Position = new Point(0, 0);

#if TEST
            string id = "minmin1985";
            string pw = "aa050117";

//             string id = "hana87";
//             string pw = "hana8798";


            driver.FindElementByXPath("//*[@id='LoginForm_username']").SendKeys(id);
            driver.FindElementByXPath("//*[@id='LoginForm_password']").SendKeys(pw + Keys.Enter);

            //             Thread.Sleep(5000);             
            //             driver.Navigate().GoToUrl("https://www.dafabookmaker.asia/kr/live-dealer ");
#endif

            return true;
        }

        public bool RefreshDefalutPage()
        {
            if (driver == null)
                return false;

            if (FocusWindow(loginTitle, true) == false)
                return false;

            driver.Navigate().Refresh();

            FocusWindow(windowTitle, true);

            return true;
        }

        public IList<IWebElement> GetGameFrameInfo()
        {
            if (FocusWindow(windowTitle, true) == false)
                return null;

            return driver.FindElements(By.ClassName("multi-window-game"));
        }

        public bool CollectRoomWiget(IList<IWebElement> listRoom_)
        {
            if (listRoom_.Count <= 0)
                return false;

            Global.dicRoomController.Clear();
            int i = 0;
            foreach(var item in listRoom_)
            {
                i++;
                driver.SwitchTo().DefaultContent();
               
                IWebElement rootFrame = item.FindElement(By.XPath(".//iframe"));
                driver.SwitchTo().Frame(rootFrame);
                IWebElement rootEle = driver.FindElement(By.ClassName("multi-window-game"));
                IWebElement roomRoot = rootEle.FindElement(By.ClassName("container--3RNV4"));

                DafaRoomWidget roomWidget = new DafaRoomWidget(rootFrame,roomRoot);
                roomWidget.driver = driver;

                Global.dicRoomController[i] = new DafaRoomController(roomWidget , i);

                if (Global.checkBox_Dollar)
                    Global.uiController.PrintRoomLog(i, "달러로 체크");
                Global.uiController.PrintRoomLog(i, "코인 수집 : " + string.Join("," ,roomWidget.dicChips.Keys.ToList().ConvertAll(x=>x.ToString()).ToArray()));
            }

            return true;
        }
    }
}
