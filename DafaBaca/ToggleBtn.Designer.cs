﻿namespace DafaBaca
{
    partial class ToggleBtn
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroButton_Toggle = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // metroButton_Toggle
            // 
            this.metroButton_Toggle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroButton_Toggle.Location = new System.Drawing.Point(0, 0);
            this.metroButton_Toggle.Name = "metroButton_Toggle";
            this.metroButton_Toggle.Size = new System.Drawing.Size(23, 22);
            this.metroButton_Toggle.TabIndex = 0;
            this.metroButton_Toggle.Text = "P";
            this.metroButton_Toggle.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroButton_Toggle.Click += new System.EventHandler(this.metroButton_Toggle_Click);
            // 
            // ToggleBtn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.metroButton_Toggle);
            this.Name = "ToggleBtn";
            this.Size = new System.Drawing.Size(23, 22);
            this.Load += new System.EventHandler(this.ToggleBtn_Load);
            this.ResumeLayout(false);

        }

        #endregion

        public MetroFramework.Controls.MetroButton metroButton_Toggle;
    }
}
