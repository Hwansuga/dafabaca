﻿namespace DafaBaca
{
    partial class DafaRoomViewer
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.metroLabel_Progress = new MetroFramework.Controls.MetroLabel();
            this.metroLabel_State = new MetroFramework.Controls.MetroLabel();
            this.metroLabel_KeyValue = new MetroFramework.Controls.MetroLabel();
            this.metroLabel_RoomName = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel_TotalBetMoney = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel_TotalRetMoney = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel_BetCnt = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel_CntStepOut = new MetroFramework.Controls.MetroLabel();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.49495F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.50505F));
            this.tableLayoutPanel1.Controls.Add(this.metroLabel8, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel7, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel_Progress, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel_State, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel_KeyValue, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel_RoomName, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel4, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel_TotalBetMoney, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel6, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel_TotalRetMoney, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel_BetCnt, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.metroLabel_CntStepOut, 1, 7);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(216, 185);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // metroLabel_Progress
            // 
            this.metroLabel_Progress.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel_Progress.AutoSize = true;
            this.metroLabel_Progress.BackColor = System.Drawing.SystemColors.Control;
            this.metroLabel_Progress.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel_Progress.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.metroLabel_Progress.Location = new System.Drawing.Point(108, 70);
            this.metroLabel_Progress.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.metroLabel_Progress.Name = "metroLabel_Progress";
            this.metroLabel_Progress.Size = new System.Drawing.Size(106, 22);
            this.metroLabel_Progress.TabIndex = 7;
            this.metroLabel_Progress.Text = "........";
            this.metroLabel_Progress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel_State
            // 
            this.metroLabel_State.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel_State.AutoSize = true;
            this.metroLabel_State.BackColor = System.Drawing.SystemColors.Control;
            this.metroLabel_State.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel_State.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.metroLabel_State.Location = new System.Drawing.Point(108, 47);
            this.metroLabel_State.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.metroLabel_State.Name = "metroLabel_State";
            this.metroLabel_State.Size = new System.Drawing.Size(106, 22);
            this.metroLabel_State.TabIndex = 6;
            this.metroLabel_State.Text = "........";
            this.metroLabel_State.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel_KeyValue
            // 
            this.metroLabel_KeyValue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel_KeyValue.AutoSize = true;
            this.metroLabel_KeyValue.BackColor = System.Drawing.SystemColors.Control;
            this.metroLabel_KeyValue.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel_KeyValue.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.metroLabel_KeyValue.Location = new System.Drawing.Point(108, 24);
            this.metroLabel_KeyValue.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.metroLabel_KeyValue.Name = "metroLabel_KeyValue";
            this.metroLabel_KeyValue.Size = new System.Drawing.Size(106, 22);
            this.metroLabel_KeyValue.TabIndex = 5;
            this.metroLabel_KeyValue.Text = "........";
            this.metroLabel_KeyValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel_RoomName
            // 
            this.metroLabel_RoomName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel_RoomName.AutoSize = true;
            this.metroLabel_RoomName.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.metroLabel_RoomName.CustomBackground = true;
            this.metroLabel_RoomName.CustomForeColor = true;
            this.metroLabel_RoomName.FontSize = MetroFramework.MetroLabelSize.Small;
            this.metroLabel_RoomName.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel_RoomName.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.metroLabel_RoomName.Location = new System.Drawing.Point(108, 1);
            this.metroLabel_RoomName.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.metroLabel_RoomName.Name = "metroLabel_RoomName";
            this.metroLabel_RoomName.Size = new System.Drawing.Size(106, 22);
            this.metroLabel_RoomName.Style = MetroFramework.MetroColorStyle.Black;
            this.metroLabel_RoomName.TabIndex = 4;
            this.metroLabel_RoomName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroLabel_RoomName.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // metroLabel4
            // 
            this.metroLabel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel4.Location = new System.Drawing.Point(2, 1);
            this.metroLabel4.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(103, 22);
            this.metroLabel4.TabIndex = 0;
            this.metroLabel4.Text = "Name";
            this.metroLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel1
            // 
            this.metroLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(2, 24);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(103, 22);
            this.metroLabel1.TabIndex = 1;
            this.metroLabel1.Text = "Round";
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel2
            // 
            this.metroLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel2.Location = new System.Drawing.Point(2, 70);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(103, 22);
            this.metroLabel2.TabIndex = 2;
            this.metroLabel2.Text = "Progress";
            this.metroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel3
            // 
            this.metroLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel3.Location = new System.Drawing.Point(2, 47);
            this.metroLabel3.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(103, 22);
            this.metroLabel3.TabIndex = 3;
            this.metroLabel3.Text = "State";
            this.metroLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel5
            // 
            this.metroLabel5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(2, 93);
            this.metroLabel5.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(103, 22);
            this.metroLabel5.TabIndex = 8;
            this.metroLabel5.Text = "배팅누적금액";
            this.metroLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel_TotalBetMoney
            // 
            this.metroLabel_TotalBetMoney.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel_TotalBetMoney.AutoSize = true;
            this.metroLabel_TotalBetMoney.BackColor = System.Drawing.SystemColors.Control;
            this.metroLabel_TotalBetMoney.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel_TotalBetMoney.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.metroLabel_TotalBetMoney.Location = new System.Drawing.Point(108, 93);
            this.metroLabel_TotalBetMoney.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.metroLabel_TotalBetMoney.Name = "metroLabel_TotalBetMoney";
            this.metroLabel_TotalBetMoney.Size = new System.Drawing.Size(106, 22);
            this.metroLabel_TotalBetMoney.TabIndex = 9;
            this.metroLabel_TotalBetMoney.Text = "........";
            this.metroLabel_TotalBetMoney.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel6
            // 
            this.metroLabel6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(2, 116);
            this.metroLabel6.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(103, 22);
            this.metroLabel6.TabIndex = 10;
            this.metroLabel6.Text = "결과누적금액";
            this.metroLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel_TotalRetMoney
            // 
            this.metroLabel_TotalRetMoney.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel_TotalRetMoney.AutoSize = true;
            this.metroLabel_TotalRetMoney.BackColor = System.Drawing.SystemColors.Control;
            this.metroLabel_TotalRetMoney.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel_TotalRetMoney.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.metroLabel_TotalRetMoney.Location = new System.Drawing.Point(108, 116);
            this.metroLabel_TotalRetMoney.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.metroLabel_TotalRetMoney.Name = "metroLabel_TotalRetMoney";
            this.metroLabel_TotalRetMoney.Size = new System.Drawing.Size(106, 22);
            this.metroLabel_TotalRetMoney.TabIndex = 11;
            this.metroLabel_TotalRetMoney.Text = "........";
            this.metroLabel_TotalRetMoney.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel7
            // 
            this.metroLabel7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(2, 139);
            this.metroLabel7.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(103, 22);
            this.metroLabel7.TabIndex = 12;
            this.metroLabel7.Text = "배팅 판수";
            this.metroLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel_BetCnt
            // 
            this.metroLabel_BetCnt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel_BetCnt.AutoSize = true;
            this.metroLabel_BetCnt.BackColor = System.Drawing.SystemColors.Control;
            this.metroLabel_BetCnt.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel_BetCnt.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.metroLabel_BetCnt.Location = new System.Drawing.Point(108, 139);
            this.metroLabel_BetCnt.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.metroLabel_BetCnt.Name = "metroLabel_BetCnt";
            this.metroLabel_BetCnt.Size = new System.Drawing.Size(106, 22);
            this.metroLabel_BetCnt.TabIndex = 13;
            this.metroLabel_BetCnt.Text = "........";
            this.metroLabel_BetCnt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel8
            // 
            this.metroLabel8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel8.Location = new System.Drawing.Point(2, 162);
            this.metroLabel8.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(103, 22);
            this.metroLabel8.TabIndex = 14;
            this.metroLabel8.Text = "Count";
            this.metroLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // metroLabel_CntStepOut
            // 
            this.metroLabel_CntStepOut.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.metroLabel_CntStepOut.AutoSize = true;
            this.metroLabel_CntStepOut.BackColor = System.Drawing.SystemColors.Control;
            this.metroLabel_CntStepOut.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel_CntStepOut.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.metroLabel_CntStepOut.Location = new System.Drawing.Point(108, 162);
            this.metroLabel_CntStepOut.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.metroLabel_CntStepOut.Name = "metroLabel_CntStepOut";
            this.metroLabel_CntStepOut.Size = new System.Drawing.Size(106, 22);
            this.metroLabel_CntStepOut.TabIndex = 15;
            this.metroLabel_CntStepOut.Text = "........";
            this.metroLabel_CntStepOut.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DafaRoomViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "DafaRoomViewer";
            this.Size = new System.Drawing.Size(216, 185);
            this.Load += new System.EventHandler(this.DafaRoomViewer_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel_RoomName;
        private MetroFramework.Controls.MetroLabel metroLabel_Progress;
        private MetroFramework.Controls.MetroLabel metroLabel_State;
        private MetroFramework.Controls.MetroLabel metroLabel_KeyValue;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel_TotalBetMoney;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel_TotalRetMoney;
        private MetroFramework.Controls.MetroLabel metroLabel_BetCnt;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel_CntStepOut;
    }
}
