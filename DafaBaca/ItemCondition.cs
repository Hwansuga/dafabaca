﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace DafaBaca
{
    public partial class ItemCondition : UserControl
    {
        List<ToggleBtn> listToggleBtn = new List<ToggleBtn>();
        public ItemCondition()
        {
            InitializeComponent();
        }

        private void ItemCondition_Load(object sender, EventArgs e)
        {
            List<FieldInfo> listField = GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).ToList();
            List<FieldInfo> listToggleFields = listField.FindAll(x => x.FieldType == typeof(ToggleBtn));

            Util.SrotByNameNymber(listToggleFields);
            foreach(var item in listToggleFields)
            {
                listToggleBtn.Add((ToggleBtn)item.GetValue(this));
            }
        }

        public List<string> GetListCondition()
        {
            List<string> listRet = new List<string>();
            foreach(var item in listToggleBtn)
            {
                listRet.Add(item.metroButton_Toggle.Text);
            }

            return listRet;
        }

        public void SetConditionUI(string[] info_)
        {
            for(int i=0; i<info_.Length; ++i)
            {
                listToggleBtn[i].Set(info_[i]);
            }
        }
    }
}
