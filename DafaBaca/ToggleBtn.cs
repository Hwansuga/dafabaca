﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DafaBaca
{
    public partial class ToggleBtn : UserControl
    {
        public string outText = "P";

        public ToggleBtn()
        {
            InitializeComponent();
        }

        private void metroButton_Toggle_Click(object sender, EventArgs e)
        {
            if (metroButton_Toggle.Text == "P")
            {
                metroButton_Toggle.Text = "B";
                metroButton_Toggle.Theme = MetroFramework.MetroThemeStyle.Dark;
            }
            else
            {
                metroButton_Toggle.Text = "P";
                metroButton_Toggle.Theme = MetroFramework.MetroThemeStyle.Light;
            }

            outText = metroButton_Toggle.Text;
        }

        public void Set(string value_)
        {
            if (value_ == "P")
            {
                metroButton_Toggle.Text = "P";
                metroButton_Toggle.Theme = MetroFramework.MetroThemeStyle.Light;
            }
            else if (value_ == "B")
            {
                metroButton_Toggle.Text = "B";
                metroButton_Toggle.Theme = MetroFramework.MetroThemeStyle.Dark;
            }
        }

        private void ToggleBtn_Load(object sender, EventArgs e)
        {

        }
    }
}
