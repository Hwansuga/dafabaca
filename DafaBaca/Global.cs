﻿using DafaBaca;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

public enum Room_STATE
{
    Room_None = -1,
    Room_BetTime = 0,
    Room_DealCard = 1,
    Room_Result = 2,
    Room_ChangeShoe = 3,
    Room_ChangeDealer = 4,
    Room_Error = 5,
}

public enum Type_Bet
{
    Nothing = -1,
    Player = 0,
    Banker = 1,
    Tie = 2,
    Type_Bet_Max = 3,
}

public enum BetWinPercent
{
    Player = 200,
    Banker = 195,
    Tie = 800,
}

public class SettingInfo
{
    public Dictionary<int, List<string>> dicCondition = new Dictionary<int, List<string>>();

    public double baseMoney = 0.0f;
    public int startStep = 0;
    public int endStep = 9;
}

class Global : Singleton<Global>
{
    public static bool entireStop = false;

    public static bool stop = false;

    public static bool goalMoneyStop = false;
    public static bool limitLoseMoneyStop = false;
    public static bool limitBetCntStop = false;
    public static bool limitStepoutCntStop = false;

    public static Form1 uiController = null;

    public static Dictionary<int, DafaRoomController> dicRoomController = new Dictionary<int, DafaRoomController>();

    public static string curOptionFile = "";

    public static SettingInfo settingInfo = new SettingInfo();

    public static bool checkBox_Real = false;
    public static bool checkBox_PreventionTimeOut = false;
    public static bool checkBox_DoubleBet = false;
    public static int numericUpDown_CntDouble = 5;
    public static int numericUpDown_TimeDouble = 1;
  
    public static bool checkBox_Dollar = false;

    public static bool checkBox_CntReTry = false;
    public static int numericUpDown_CntReTry = 1;

    public static bool checkBox_RetryDelay = false;
    public static int numericUpDown_RetryDelay = 30;

    public static bool checkBox_GoalMoney = false;
    public static int numericUpDown_GoalMoney = 100000;
    public static bool checkBox_LimitLoseMoney = false;
    public static int numericUpDown_LimitLoseMoney = 100000;

    public static bool checkBox_LmitBetCnt = false;
    public static int numericUpDown_LmitBetCnt = 50;

    public static bool checkBox_LmitStepoutCnt = false;
    public static int numericUpDown_LmitStepoutCnt = 1;

    public static bool checkBox_LimitBetNum = false;
    public static int numericUpDown_LimitBetNum = 1;

    public static bool checkBox_SideBet = false;


    public static string ConvertRoomStateToString(Room_STATE state_)
    {
        switch(state_)
        {
            case Room_STATE.Room_BetTime:
                return "배팅타임";
            case Room_STATE.Room_DealCard:
                return "카드배포";
            case Room_STATE.Room_Result:
                return "결과산출";
            case Room_STATE.Room_Error:
                return "알수없는상태";
        }
        return "";
    }

    public static string GetStringRetBitArray(BitArray ret_)
    {
        string strRet = "";
        for (int i = 0; i < (int)Type_Bet.Type_Bet_Max; ++i)
        {
            if (ret_.Get(i))
                strRet += ((Type_Bet)i).ToString() + " ";
        }

        return strRet;
    }

    public static bool AllStop()
    {
        if (dicRoomController.Values.ToList().FindAll(x => x.stop == false).Count > 0)
            return false;
        return true;
    }

    public static double GetWinMoney(double betMoney_, BitArray bet_, BitArray ret_)
    {
        BitArray compare = (BitArray)bet_.Clone();
        compare.And(ret_);

        double winMoney = 0;
        for (int i = 0; i < compare.Count; ++i)
        {
            if (bet_[i] == false)
                continue;

            winMoney -= betMoney_;

            if (compare[i] == false)
                continue;

            BetWinPercent winPercent = Util.ToEnum<BetWinPercent>(((Type_Bet)i).ToString());

            winMoney += ((betMoney_ * (double)winPercent) / 100.0f);
        }

        //if ret is tie ... return player,banker bet money
        if (ret_.Get((int)Type_Bet.Tie))
        {
            if (bet_.Get((int)Type_Bet.Player))
                winMoney += betMoney_;
            if (bet_.Get((int)Type_Bet.Banker))
                winMoney += betMoney_;
        }


        return winMoney;
    }

    public static BitArray MakeBitArr(Type_Bet typeBet_)
    {
        BitArray ret = new BitArray((int)Type_Bet.Type_Bet_Max, false);

        if (typeBet_ != Type_Bet.Nothing)
            ret.Set((int)typeBet_, true);

        return ret;
    }

    public static BitArray MakeBitArr(string bet_)
    {
        Type_Bet bet = Util.ToEnum<Type_Bet>(bet_);
        return MakeBitArr(bet);
    }

    public static List<string> ChangeSetInfoToString(SettingInfo info_)
    {
        List<string> listRet = new List<string>();

        for (int i = 0; i < 10; ++i)
        {
            string temp = "c/" + i + "/";
            if (info_.dicCondition.Keys.Contains(i))
            {
                temp += "True/" + string.Join(";", info_.dicCondition[i].ToArray());
            }
            else
            {
                temp += "False/P;P;P;P;P;P;P;P;P;P";
            }

            listRet.Add(temp);
        }

        string rule = "r/" + info_.startStep;
        listRet.Add(rule);

        string num = "n/" + info_.endStep;
        listRet.Add(num);

        string baseMoney = "b/" + info_.baseMoney;
        listRet.Add(baseMoney);

        return listRet;
    }

    public static string SaveSettingInfo(SettingInfo info_)
    {
        string fileName = "";

        string path = Directory.GetCurrentDirectory();
        path += "\\" + "Option";

        if (Directory.Exists(path) == false)
            Directory.CreateDirectory(path);

        SaveFileDialog ofd = new SaveFileDialog();
        ofd.InitialDirectory = path;
        ofd.Title = "설정";
        ofd.Filter = "|*.bofs";

        DialogResult dr = ofd.ShowDialog();

        if (dr == DialogResult.OK)
        {
            fileName = System.IO.Path.GetFileName(ofd.FileName);
            List<string> listInfo = ChangeSetInfoToString(info_);
            Util.SaveTxtFile(fileName.Replace(".bofs", "") + ".bofs", listInfo, "Option");
            Global.curOptionFile = fileName;
        }
        else if (dr == DialogResult.Cancel)
        {

        }

        return fileName;
    }

    public static void LoadSettingFromFile(string fileName_)
    {
        List<string> listInfo = new List<string>();
        Util.LoadTxtFile(fileName_, ref listInfo, "Option");

        foreach (var item in listInfo)
        {
            string[] arrInfo = item.Split('/');
            if (arrInfo.Length <= 0)
                continue;

            switch (arrInfo[0])
            {
                case "c":
                    {
                        int i = int.Parse(arrInfo[1]);
                        bool enableInfo = bool.Parse(arrInfo[2]);
                        if (enableInfo == false)
                            break;

                        string[] conditionInfo = arrInfo[3].Split(';');
                        settingInfo.dicCondition[i] = conditionInfo.ToList();
                    }
                    break;
                case "r":
                    {
                        int index = int.Parse(arrInfo[1]);
                        settingInfo.startStep = index;
                    }
                    break;
                case "n":
                    {
                        int num = int.Parse(arrInfo[1]);
                        settingInfo.endStep = num;
                    }
                    break;
                case "b":
                    {
                        int index = int.Parse(arrInfo[1]);
                        settingInfo.baseMoney = index;
                    }
                    break;
            }
        }

    }
}

