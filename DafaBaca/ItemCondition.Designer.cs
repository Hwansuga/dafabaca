﻿namespace DafaBaca
{
    partial class ItemCondition
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.toggleBtn1 = new DafaBaca.ToggleBtn();
            this.toggleBtn2 = new DafaBaca.ToggleBtn();
            this.toggleBtn3 = new DafaBaca.ToggleBtn();
            this.toggleBtn4 = new DafaBaca.ToggleBtn();
            this.toggleBtn5 = new DafaBaca.ToggleBtn();
            this.toggleBtn6 = new DafaBaca.ToggleBtn();
            this.toggleBtn7 = new DafaBaca.ToggleBtn();
            this.toggleBtn8 = new DafaBaca.ToggleBtn();
            this.toggleBtn9 = new DafaBaca.ToggleBtn();
            this.toggleBtn10 = new DafaBaca.ToggleBtn();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 10;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.Controls.Add(this.toggleBtn10, 9, 0);
            this.tableLayoutPanel1.Controls.Add(this.toggleBtn9, 8, 0);
            this.tableLayoutPanel1.Controls.Add(this.toggleBtn8, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.toggleBtn7, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.toggleBtn6, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.toggleBtn5, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.toggleBtn4, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.toggleBtn3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.toggleBtn2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.toggleBtn1, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(231, 23);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // toggleBtn1
            // 
            this.toggleBtn1.Location = new System.Drawing.Point(0, 0);
            this.toggleBtn1.Margin = new System.Windows.Forms.Padding(0);
            this.toggleBtn1.Name = "toggleBtn1";
            this.toggleBtn1.Size = new System.Drawing.Size(23, 22);
            this.toggleBtn1.TabIndex = 1;
            // 
            // toggleBtn2
            // 
            this.toggleBtn2.Location = new System.Drawing.Point(23, 0);
            this.toggleBtn2.Margin = new System.Windows.Forms.Padding(0);
            this.toggleBtn2.Name = "toggleBtn2";
            this.toggleBtn2.Size = new System.Drawing.Size(23, 22);
            this.toggleBtn2.TabIndex = 2;
            // 
            // toggleBtn3
            // 
            this.toggleBtn3.Location = new System.Drawing.Point(46, 0);
            this.toggleBtn3.Margin = new System.Windows.Forms.Padding(0);
            this.toggleBtn3.Name = "toggleBtn3";
            this.toggleBtn3.Size = new System.Drawing.Size(23, 22);
            this.toggleBtn3.TabIndex = 3;
            // 
            // toggleBtn4
            // 
            this.toggleBtn4.Location = new System.Drawing.Point(69, 0);
            this.toggleBtn4.Margin = new System.Windows.Forms.Padding(0);
            this.toggleBtn4.Name = "toggleBtn4";
            this.toggleBtn4.Size = new System.Drawing.Size(23, 22);
            this.toggleBtn4.TabIndex = 4;
            // 
            // toggleBtn5
            // 
            this.toggleBtn5.Location = new System.Drawing.Point(92, 0);
            this.toggleBtn5.Margin = new System.Windows.Forms.Padding(0);
            this.toggleBtn5.Name = "toggleBtn5";
            this.toggleBtn5.Size = new System.Drawing.Size(23, 22);
            this.toggleBtn5.TabIndex = 5;
            // 
            // toggleBtn6
            // 
            this.toggleBtn6.Location = new System.Drawing.Point(115, 0);
            this.toggleBtn6.Margin = new System.Windows.Forms.Padding(0);
            this.toggleBtn6.Name = "toggleBtn6";
            this.toggleBtn6.Size = new System.Drawing.Size(23, 22);
            this.toggleBtn6.TabIndex = 6;
            // 
            // toggleBtn7
            // 
            this.toggleBtn7.Location = new System.Drawing.Point(138, 0);
            this.toggleBtn7.Margin = new System.Windows.Forms.Padding(0);
            this.toggleBtn7.Name = "toggleBtn7";
            this.toggleBtn7.Size = new System.Drawing.Size(23, 22);
            this.toggleBtn7.TabIndex = 7;
            // 
            // toggleBtn8
            // 
            this.toggleBtn8.Location = new System.Drawing.Point(161, 0);
            this.toggleBtn8.Margin = new System.Windows.Forms.Padding(0);
            this.toggleBtn8.Name = "toggleBtn8";
            this.toggleBtn8.Size = new System.Drawing.Size(23, 22);
            this.toggleBtn8.TabIndex = 8;
            // 
            // toggleBtn9
            // 
            this.toggleBtn9.Location = new System.Drawing.Point(184, 0);
            this.toggleBtn9.Margin = new System.Windows.Forms.Padding(0);
            this.toggleBtn9.Name = "toggleBtn9";
            this.toggleBtn9.Size = new System.Drawing.Size(23, 22);
            this.toggleBtn9.TabIndex = 9;
            // 
            // toggleBtn10
            // 
            this.toggleBtn10.Location = new System.Drawing.Point(207, 0);
            this.toggleBtn10.Margin = new System.Windows.Forms.Padding(0);
            this.toggleBtn10.Name = "toggleBtn10";
            this.toggleBtn10.Size = new System.Drawing.Size(23, 22);
            this.toggleBtn10.TabIndex = 10;
            // 
            // ItemCondition
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ItemCondition";
            this.Size = new System.Drawing.Size(238, 29);
            this.Load += new System.EventHandler(this.ItemCondition_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private ToggleBtn toggleBtn10;
        private ToggleBtn toggleBtn9;
        private ToggleBtn toggleBtn8;
        private ToggleBtn toggleBtn7;
        private ToggleBtn toggleBtn6;
        private ToggleBtn toggleBtn5;
        private ToggleBtn toggleBtn4;
        private ToggleBtn toggleBtn3;
        private ToggleBtn toggleBtn2;
        private ToggleBtn toggleBtn1;
    }
}
