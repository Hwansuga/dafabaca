﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DafaBaca;
using System.Collections;

class BetCommander : Singleton<BetCommander>
{

    public static int GetBetNumber(int num_)
    {
        int intervalNum = num_ % Global.settingInfo.dicCondition.Count;
        return Global.settingInfo.dicCondition.Keys.ElementAt(intervalNum);
    }

    public static Type_Bet CustomBet(int num_, int step_)
    {
        int intervalNum = num_ % Global.settingInfo.dicCondition.Count;

        List<string> listCon = Global.settingInfo.dicCondition.Values.ElementAt(intervalNum);

        if (listCon[step_] == "P")
        {
            return Type_Bet.Player;
        }
        else if (listCon[step_] == "B")
        {
            return Type_Bet.Banker;
        }

        return Type_Bet.Nothing;
    } 

    public static double BetMoney(int step_)
    {
        int interverStep = step_ - Global.settingInfo.startStep;
        if (interverStep < 0)
            return 0;

        if (interverStep == 0)
            return Global.settingInfo.baseMoney;

        double ret = Global.settingInfo.baseMoney;

        for(int i=0; i< interverStep; ++i)
        {
            ret = ret * 2 + Global.settingInfo.baseMoney;
        }

        return ret;
    }

    public static void WinNextStep(DafaRoomController room_)
    {      
        if (Global.goalMoneyStop)
        {
            Global.uiController.PrintRoomLog(room_.roomNum, "목표수익 도달 정지");
            room_.stop = true;
            return;
        }

        if (Global.limitLoseMoneyStop)
        {
            Global.uiController.PrintRoomLog(room_.roomNum, "한계손실 도달 정지");
            room_.stop = true;
            return;
        }

        if (Global.limitBetCntStop)
        {
            Global.uiController.PrintRoomLog(room_.roomNum, "제한판수 도달 정지");
            room_.stop = true;
            return;
        }

        if (Global.limitStepoutCntStop)
        {
            Global.uiController.PrintRoomLog(room_.roomNum, "제한카운트 초과 정지");
            room_.stop = true;
            return;
        }

        room_.betNum++;
        room_.betStep = 0;
        if (room_.doDoubleBet)
        {
            if (room_.cntDoubleBet >= Global.numericUpDown_CntDouble)
            {
                room_.doDoubleBet = false;
                Global.uiController.PrintRoomLog(room_.roomNum, room_.cntDoubleBet + " 슈퍼더블베팅 끝");
                room_.cntDoubleBet = 1;
                room_.timeDoubleBet = 1;
            }
            else
            {
                room_.cntDoubleBet++;
            }
        }
    }

    public static void LoseNextStep(DafaRoomController room_)
    {
        if (room_.betStep >= (Global.settingInfo.endStep-1))
        {
                        
            if (Global.goalMoneyStop)
            {
                Global.uiController.PrintRoomLog(room_.roomNum, "목표수익 도달 정지");
                room_.stop = true;
                return;
            }

            if (Global.limitLoseMoneyStop)
            {
                Global.uiController.PrintRoomLog(room_.roomNum, "한계손실 도달 정지");
                room_.stop = true;
                return;
            }

            if (Global.limitBetCntStop)
            {
                Global.uiController.PrintRoomLog(room_.roomNum, "제한판수 도달 정지");
                room_.stop = true;
                return;
            }

            if (Global.limitStepoutCntStop)
            {
                Global.uiController.PrintRoomLog(room_.roomNum, "제한카운트 초과 정지");
                room_.stop = true;
                return;
            }

            room_.betNum++;
            room_.betStep = 0;
            room_.cntStepOut++;
                        
            if (room_.doDoubleBet)
            {
                if (room_.timeDoubleBet < Global.numericUpDown_TimeDouble)
                {
                    room_.timeDoubleBet++;
                    room_.cntDoubleBet = 1;
                }
                else
                {
                    room_.doDoubleBet = false;
                    Global.uiController.PrintRoomLog(room_.roomNum, room_.cntDoubleBet + " 슈퍼더블베팅 끝");
                    room_.cntDoubleBet = 1;                
                }              
            }
            else
            {
                room_.doDoubleBet = Global.checkBox_DoubleBet;
                if (room_.doDoubleBet == false)
                {
                    Global.uiController.PrintRoomLog(room_.roomNum, room_.cntDoubleBet + " 슈퍼더블베팅 끝");                  
                }
                else
                {
                    Global.uiController.PrintRoomLog(room_.roomNum, "슈퍼더블베팅 시작");
                }

                room_.cntDoubleBet = 1;
                room_.timeDoubleBet = 1;

            }
        }
        else
        {
            room_.betStep++;
            if (room_.doDoubleBet)
                room_.cntDoubleBet++;
        }
    }
}

